const router = require('koa-router')()
let Mock = require('mockjs');

// table
router.get('/high/list', async (ctx, next) => {
  ctx.body = Mock.mock({
    "code": 0,
    "message": "",
    "result": {
      "list|10": [{
        "id|+1": 1,
        "username": "@cname",
        "sex|1-2": 1,
        "state|0-4": 1,
        "age|18-60": 18,
        "interest|0-7": 0,
        "isMarried|0-1": 1,
        "birthday": "2000-01-01",
        "address": "北京市海淀区",
        "time": "09:00:00"
      }],
      page: 1,
      page_size: 10,
      total_count: 30
    }
  })
})

module.exports = router