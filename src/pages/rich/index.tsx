import { Button, Card, Modal } from 'antd'
import draftjs from 'draftjs-to-html';
import React, { Component } from 'react'
import { Editor } from 'react-draft-wysiwyg';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import { IProps, IState } from "./types";

export default class Rich extends Component<IProps, IState> {

  public state: IState
  constructor(props: Readonly<IProps>) {
    super(props);
    this.state = {
      showRichText: false, // 是否显示状态弹框
      editorContent: '', // 文本数据
      editorState: '', // 编辑器状态数据
    };
  }

  /**
   * 清除内容
   */
  public handleClearContent = () => {
    this.setState({
      editorState: ''
    })
  }

  /**
   * 获取富文本的内容
   */
  public handleGetText = () => {
    this.setState({
      showRichText: true
    })
  }


  /**
   * 编辑器内容变更
   * @param {String} editorContent 数据
   */
  public onEditorChange = (editorContent:any) => {
    this.setState({
      editorContent,
    });
  };

  /**
   * 编辑器状态变更
   */
  public onEditorStateChange = (editorState:any) => {
    this.setState({
      editorState
    });
  };

  public render() {
    const { editorState } = this.state;
    return (
      <div>
        <Card style={ { marginTop: 10 } }>
          <Button type="primary" onClick={ this.handleClearContent }>清空内容</Button>
          <Button type="primary" onClick={ this.handleGetText }>获取HTML文本</Button>
        </Card>
        <Card title="富文本编辑器">
          <Editor
            editorState={ editorState }
            onContentStateChange={ this.onEditorChange }
            onEditorStateChange={ this.onEditorStateChange }
          />
        </Card>
        <Modal
          title="富文本"
          visible={ this.state.showRichText }
          onCancel={ () => {
            this.setState({
              showRichText: false
            })
          } }
          footer={ null }
        >
          { draftjs(this.state.editorContent) }
        </Modal>
      </div>
    )
  }
}