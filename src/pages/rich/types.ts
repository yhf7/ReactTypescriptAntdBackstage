export interface IProps extends Typings.RouteProps<any, any> {
  
}

export interface IState {
  showRichText: boolean
  editorContent: any
  editorState: any
}