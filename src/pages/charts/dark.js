
var contrastColor = '#eee';
var axisCommon = function () {
    return {
        axisLabel: {
            textStyle: {
                color: contrastColor
            }
        },
        axisLine: {
            lineStyle: {
                color: contrastColor
            }
        },
        axisTick: {
            lineStyle: {
                color: contrastColor
            }
        },
        splitArea: {
            areaStyle: {
                color: contrastColor
            }
        },
        splitLine: {
            lineStyle: {
                color: '#aaa',
                type: 'dashed',
            }
        },

    };
};

var colorPalette = ['#dd6b66', '#759aa0', '#e69d87', '#8dc1a9', '#ea7e53', '#eedd78', '#73a373', '#73b9bc', '#7289ab', '#91ca8c', '#f49f42'];

export default {
    backgroundColor: '#333',
    candlestick: {
        itemStyle: {
            normal: {
                borderColor: '#FD1050',
                borderColor0: '#0CF49B',
                color: '#FD1050',
                color0: '#0CF49B',
            }
        }
    },
    categoryAxis: axisCommon(),
    color: colorPalette,
    dataZoom: {
        textStyle: {
            color: contrastColor
        }
    },
    gauge: {
        title: {
            textStyle: {
                color: contrastColor
            }
        }
    },
    graph: {
        color: colorPalette
    },
    legend: {
        textStyle: {
            color: contrastColor
        }
    },
    line: {
        symbol: 'circle'
    },
    logAxis: axisCommon(),
    textStyle: {
        color: contrastColor
    },
    timeAxis: axisCommon(),
    timeline: {
        controlStyle: {
            normal: {
                borderColor: contrastColor,
                color: contrastColor,
            }
        },
        itemStyle: {
            normal: {
                color: colorPalette[1]
            }
        },
        label: {
            normal: {
                textStyle: {
                    color: contrastColor
                }
            }
        },
        lineStyle: {
            color: contrastColor
        },
       
    },
    title: {
        textStyle: {
            color: contrastColor
        }
    },
    toolbox: {
        iconStyle: {
            normal: {
                borderColor: contrastColor
            }
        }
    },
    tooltip: {
        axisPointer: {
            crossStyle: {
                color: contrastColor
            },
            lineStyle: {
                color: contrastColor
            },
        }
    },
    
    valueAxis: axisCommon(),
}