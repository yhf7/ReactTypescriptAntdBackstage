export default {
    "backgroundColor": "#ffffff",
    "categoryAxis": {
        "axisLabel": {
            "show": true,
            "textStyle": {
                "color": "#999999",
                "fontSize": "14"
            },
            
        },
        "axisLine": {
            "lineStyle": {
                "color": "#f1f3f5"
            },
            "show": true,
        },
        "axisTick": {
            "lineStyle": {
                "color": "#f1f3f5"
            },
            "show": true,
           
        },
        "splitArea": {
            "areaStyle": {
                "color": [
                    "rgba(250,250,250,0.3)",
                    "rgba(200,200,200,0.3)"
                ]
            },
            "show": false,
        },
        "splitLine": {
            "lineStyle": {
                "color": [
                    "#f1f3f5"
                ]
            },
            "show": true,
        },
    },
    "color": [
        "#f9c700",
        "#ff5400",
        "#6699cc",
        "#9cb3c5",
        "#e0e6ec",
        "#666666",
        "#787464",
        "#cc7e63",
        "#724e58",
        "#4b565b"
    ],
    "legend": {
        "textStyle": {
            "color": "#333333"
        }
    },
    "line": {
        "itemStyle": {
            "normal": {
                "borderWidth": 1
            }
        },
        "lineStyle": {
            "normal": {
                "width": 2
            }
        },
        "smooth": false,
        "symbol": "emptyCircle",
        "symbolSize": "10",

    },
    "markPoint": {
        "label": {
            "emphasis": {
                "textStyle": {
                    "color": "#ffffff"
                }
            },
            "normal": {
                "textStyle": {
                    "color": "#ffffff"
                }
            },
            
        }
    },
    "pie": {
        "itemStyle": {
            "emphasis": {
                "borderColor": "#ccc",
                "borderWidth": 0,
                
            },
            "normal": {
                "borderColor": "#ccc",
                "borderWidth": 0,
                
            },
            
        }
    },
    
    
    "textStyle": {},
    "timeline": {
        "checkpointStyle": {
            "borderColor": "rgba(194,53,49,0.5)",
            "color": "#e43c59",

        },
        "controlStyle": {
            "emphasis": {
                "borderColor": "#293c55",
                "borderWidth": 0.5,
                "color": "#293c55",

            },
            "normal": {
                "borderColor": "#293c55",
                "borderWidth": 0.5,
                "color": "#293c55",
                
            },
            
        },
        "itemStyle": {
            "emphasis": {
                "color": "#a9334c"
            },
            "normal": {
                "borderWidth": 1,
                "color": "#293c55",

            },
            
        },
        "label": {
            "emphasis": {
                "textStyle": {
                    "color": "#293c55"
                }
            },
            "normal": {
                "textStyle": {
                    "color": "#293c55"
                }
            },
            
        },
        "lineStyle": {
            "color": "#293c55",
            "width": 1
        },
        
    },
    "title": {
        "subtextStyle": {
            "color": "#cccccc"
        },
        "textStyle": {
            "color": "#cccccc"
        },
    },
    
    "toolbox": {
        "iconStyle": {
            "emphasis": {
                "borderColor": "#666666"
            },
            "normal": {
                "borderColor": "#999999"
            },
        }
    },
    "tooltip": {
        "axisPointer": {
            "crossStyle": {
                "color": "#cccccc",
                "width": 1
            },
            "lineStyle": {
                "color": "#cccccc",
                "width": 1
            },

        }
    },
    
    "valueAxis": {
        "axisLabel": {
            "show": true,
            "textStyle": {
                "color": "#999999",
                "fontSize": "14"
            },
        },
        "axisLine": {
            "lineStyle": {
                "color": "#f1f3f5"
            },
            "show": true,
        },
        "axisTick": {
            "lineStyle": {
                "color": "#f1f3f5"
            },
            "show": true,
            
        },
        "splitArea": {
            "areaStyle": {
                "color": [
                    "rgba(250,250,250,0.3)",
                    "rgba(200,200,200,0.3)"
                ]
            },
            "show": false,
        },
        "splitLine": {
            "lineStyle": {
                "color": [
                    "#f1f3f5"
                ]
            },
            "show": true,
            
        },
        
    },
    
}