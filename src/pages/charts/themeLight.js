var colorPalette = [
    '#C1232B',
    '#27727B',
    '#FCCE10',
    '#E87C25',
    '#B5C334',
    '#FE8463',
    '#9BCA63',
    '#FAD860',
    '#F3A43B',
    '#60C0DD',
    '#D7504B',
    '#C6E579',
    '#F4E001',
    '#F0805A',
    '#26C0C0'
];
export default {

    color: colorPalette,

    title: {
        textStyle: {
            color: '#27727B',
            fontWeight: 'normal',

        }
    },

    visualMap: {
        color: ['#C1232B', '#FCCE10']
    },

    toolbox: {
        iconStyle: {
            normal: {
                borderColor: colorPalette[0]
            }
        }
    },

    tooltip: {
        axisPointer: {
            crossStyle: {
                color: '#27727B'
            },
            lineStyle: {
                color: '#27727B',
                type: 'dashed'
            },
            shadowStyle: {
                color: 'rgba(200,200,200,0.3)'
            },
            type: 'line',

        },
        backgroundColor: 'rgba(50,50,50,0.5)',

    },

    dataZoom: {
        dataBackgroundColor: 'rgba(181,195,52,0.3)',
        fillerColor: 'rgba(181,195,52,0.2)',
        handleColor: '#27727B'
    },

    categoryAxis: {
        axisLine: {
            lineStyle: {
                color: '#27727B'
            }
        },
        splitLine: {
            show: false
        }
    },

    valueAxis: {
        axisLine: {
            show: false
        },
        splitArea: {
            show: false
        },
        splitLine: {
            lineStyle: {
                color: ['#ccc'],
                type: 'dashed'
            }
        }
    },

    timeline: {
        controlStyle: {
            normal: {
                borderColor: '#27727B',
                color: '#27727B',

            }
        },
        lineStyle: {
            color: '#27727B'
        },

        symbol: 'emptyCircle',
        symbolSize: 3
    },

    line: {
        itemStyle: {
            emphasis: {
                borderWidth: 0
            },
            normal: {
                borderColor: '#fff',
                borderWidth: 2,

                lineStyle: {
                    width: 3
                }
            },

        },
        symbol: 'circle',
        symbolSize: 3.5
    },

    candlestick: {
        itemStyle: {
            normal: {
                color: '#C1232B',
                color0: '#B5C334',
                lineStyle: {

                    color: '#C1232B',
                    color0: '#B5C334',
                    width: 1,
                }
            }
        }
    },

    graph: {
        color: colorPalette
    },

    map: {
        itemStyle: {
            emphasis: {
                areaColor: '#fe994e'
            },
            normal: {
                areaColor: '#ddd',
                borderColor: '#eee'
            },
            
        },
        label: {
            emphasis: {
                textStyle: {
                    color: 'rgb(100,0,0)'
                }
            },
            normal: {
                textStyle: {
                    color: '#C1232B'
                }
            },
            
        },
        
    },

    gauge: {
        axisLabel: {
            textStyle: {
                color: '#fff'
            }
        },
        axisLine: {
            lineStyle: {
                color: [
                    [
                        0.2, '#B5C334'
                    ],
                    [
                        0.8, '#27727B'
                    ],
                    [1, '#C1232B']
                ]
            }
        },
        axisTick: {
            length: 5,
            lineStyle: {
                color: '#fff'
            },
            splitNumber: 2,
            
        },
        splitLine: {
            length: '5%',
            lineStyle: {
                color: '#fff'
            }
        },
        title: {
            offsetCenter: [0, -20]
        }
    }
}