/* eslint-disable no-dupe-keys */

import { Card } from 'antd'
// 组件化 图表语法插件
import ReactEcharts from "echarts-for-react";
// 导入饼图
import 'echarts/lib/chart/pie'
import 'echarts/lib/component/legend'
import 'echarts/lib/component/markPoint'
import 'echarts/lib/component/title'
import 'echarts/lib/component/tooltip'
// 按需加载
import echarts from 'echarts/lib/echarts'
import React, { Component } from 'react'
// 引入主题色
import dark from '../dark'

export default class Bar extends Component {

  public componentDidMount() {
    // 加载注入
    echarts.registerTheme('Imooc', dark);
  }

  public getOption = () => {
    const option = {
      title: {
        x: 'center',
        text: '用户骑行订单'
      },
      legend: {
        orient: 'vertical',
        right: 10,
        top: 20,
        bottom: 20,
        data: ['周一', '周二', '周三', '周四', '周五', '周六', '周日']
      },
      series: [
        {
          name: '订单量',
          type: 'pie',
          data: [
            {
              value: 1000,
              name: '周一'
            },
            {
              value: 1000,
              name: '周二'
            },
            {
              value: 2000,
              name: '周三'
            },
            {
              value: 1500,
              name: '周四'
            },
            {
              value: 3000,
              name: '周五'
            },
            {
              value: 200,
              name: '周六'
            },
            {
              value: 1200,
              name: '周日'
            },
          ]
        }
      ],
      tooltip: {
        trigger: 'item',
        formatter: '{a}<br/>{b}:{c}({d}%)',
        normal: {
          show: false
        }
      }
    };
    return option
  }

  public getOption2 = () => {
    const option = {
      title: {
        x: 'center',
        text: '用户骑行订单'
      },
      legend: {
        orient: 'vertical',
        right: 10,
        top: 20,
        bottom: 20,
        data: ['周一', '周二', '周三', '周四', '周五', '周六', '周日']
      },
      series: [
        {
          name: '订单量',
          type: 'pie',
          radius: ['50%', '70%'],
          data: [
            {
              value: 1000,
              name: '周一'
            },
            {
              value: 1000,
              name: '周二'
            },
            {
              value: 2000,
              name: '周三'
            },
            {
              value: 1500,
              name: '周四'
            },
            {
              value: 3000,
              name: '周五'
            },
            {
              value: 200,
              name: '周六'
            },
            {
              value: 1200,
              name: '周日'
            },
          ]
        }
      ],
      tooltip: {
        trigger: 'item',
        formatter: '{a}<br/>{b}:{c}({d}%)',
        normal: {
          show: false
        }
      }
    };
    return option
  }

  public getOption3 = () => {
    const option = {
      title: {
        x: 'center',
        text: '用户骑行订单'
      },
      legend: {
        orient: 'vertical',
        right: 10,
        top: 20,
        bottom: 20,
        data: ['周一', '周二', '周三', '周四', '周五', '周六', '周日']
      },
      series: [
        {
          name: '订单量',
          type: 'pie',
          roseType: 'radius',
          data: [
            {
              value: 1000,
              name: '周一'
            },
            {
              value: 1000,
              name: '周二'
            },
            {
              value: 2000,
              name: '周三'
            },
            {
              value: 1500,
              name: '周四'
            },
            {
              value: 3000,
              name: '周五'
            },
            {
              value: 200,
              name: '周六'
            },
            {
              value: 1200,
              name: '周日'
            }
          ].sort( (a, b) => a.value - b.value),
          animationType: 'scale',
          animationEasing: 'elasticOut',
          animationDelay () {
            return Math.random() * 200;
          }
        },
      ],
      tooltip: {
        trigger: 'item',
        formatter: '{a}<br/>{b}:{c}({d}%)',
        normal: {
          show: false
        }
      }
    };
    return option
  }

  public render() {
    return (
      <div>
        <Card title="饼图表之一">
          <ReactEcharts option={ this.getOption() } theme="Imooc" style={ { height: 500 } } />
        </Card>
        <Card title="饼图表之二" style={ { marginTop: 20 } }>
          <ReactEcharts option={ this.getOption2() } theme="Imooc" style={ { height: 500 } } />
        </Card>
        <Card title="饼图表之三" style={ { marginTop: 20 } }>
          <ReactEcharts option={ this.getOption3() } theme="Imooc" style={ { height: 500 } } />
        </Card>
      </div>
    )
  }
}