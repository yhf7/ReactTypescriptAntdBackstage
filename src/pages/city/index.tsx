/**
 * 城市管理页面
 * 1. 头部点击栏，按钮select选择按钮
 * 2. 点击创建城市，显示弹框表单
 * 3. 表格显示城市数据
 */
import API from "@/API";
import BaseForm from '@/components/BaseForm'
import OpenCityForm from "@/components/OpenCityForm";
import Utils from '@/utils/utils';
import { Button, Card, message, Modal, Table } from 'antd'
import React, { Component } from 'react';
import { IParams,IProps, IState } from "./types";


export default class City extends Component<IProps, IState> {

  public state: IState
  public params: IParams
  public formList: object[];
  public cityForm:any;
  constructor(props: Readonly<IProps>) {
    super(props);
    this.state = {
      list: [],
      isShowOpenCity: false // 弹框显示状态
    };
    // 自定义params数据，请求时默认数据
    this.params = {
      page: 1
    }
    // 组件数据
    this.formList = [
      {
        type: 'SELECT',
        label: '城市',
        field: 'city_id',
        placeholder: '全部',
        initialValue: '0',
        width: 100,
        list: [{ 'id': "0", name: '全部' }, { 'id': "1", name: '广东' }, { 'id': "2", name: '广州' }, { 'id': "3", name: '深圳' }]
      },
      {
        type: 'SELECT',
        field: 'mode',
        label: '用车模式',
        placeholder: '全部',
        initialValue: '0',
        width: 120,
        list: [{ id: '0', name: '全部' }, { id: '1', name: '指定停车点模式' }, { id: '2', name: '禁停区模式' }]
      },
      {
        type: 'SELECT',
        field: 'op_mode',
        label: '营运模式',
        placeholder: '全部',
        initialValue: '0',
        width: 80,
        list: [{ id: '0', name: '全部' }, { id: '1', name: '自营' }, { id: '2', name: '加盟' }]
      },
      {
        type: 'SELECT',
        field: 'auth_status',
        label: '加盟商授权状态',
        placeholder: '全部',
        initialValue: '0',
        width: 100,
        list: [{ id: '0', name: '全部' }, { id: '1', name: '已授权' }, { id: '2', name: '未授权' }]
      },
    ]
  }
  

  // 执行完dom操作后运行
  public componentDidMount() {
    // 默认调用请求数据
    this.requestList();
  }

  // 开通城市
  public handleOpenCity = () => {
    this.setState({
      isShowOpenCity: true// modal开关
    })
  }

  /**
   * 开通城市提交处理
   */
  public handleSubmit = () => {
    // 获取通过cityform传过来的表单数据
    const cityInfo = this.cityForm.props.form.getFieldsValue();
    // 请求api获取数据，传入params
    API.getCityOpen({
      data: {
        params: cityInfo
      },
      isMock: true
    }).then((res:any) => {
      // 请求数据成功设置提示和弹框关闭
      if (res.code == 0) {
        message.success('开通成功');
        this.setState({
          isShowOpenCity: false
        })
        // 重新请求api刷新页面s
        this.requestList();
      }
    })
  }

  // 默认请求我们的接口数据
  public requestList = () => {
    // 调用公共封装的列表请求函数
    API.getOpenCity(this, this.params, true)
  }

  // 子组件调用方法
  /**
   * 子组件点击查询
   */
  public handleFilter = (params: IParams) => {
    this.params = params
    this.requestList()
  }

  public render() {
    // 表格模型
    const columns = [
      {
        title: '城市ID',
        dataIndex: 'id'
      }, {
        title: '城市名称',
        dataIndex: 'name'
      }, {
        title: '用车模式',
        dataIndex: 'mode',
        render(mode:number) {
          return mode === 1 ? '停车点' : '禁停区';
        }
      }, {
        title: '营运模式',
        dataIndex: 'op_mode',
        render(opMode:number) {
          return opMode === 1 ? '自营' : '加盟';
        }
      }, {
        title: '授权加盟商',
        dataIndex: 'franchisee_name'
      }, {
        title: '城市管理员',
        dataIndex: 'city_admins',
        render(arr:object[]) {
          return arr.map((item:{user_name?:string}) => {
            return item.user_name;
          }).join(',');
        }
      }, {
        title: '城市开通时间',
        dataIndex: 'open_time'
      }, {
        title: '操作时间',
        dataIndex: 'update_time',
        render: Utils.formateDate
      }, {
        title: '操作人',
        dataIndex: 'sys_user_name'
      }
    ]

    return (
      <div>
        {/* 头部 */ }
        <Card>
          {/* 调用组件 */ }
          <BaseForm filterSubmit={ this.handleFilter } formList={ this.formList } />
        </Card>
        {/* 中间按钮 */ }
        <Card style={ { marginTop: 10 } }>
          <Button type="primary" onClick={ this.handleOpenCity }>开通城市</Button>
        </Card>
        {/* 表格内容 */ }
        <div className="content-wrap">
          <Table
            bordered={true}
            columns={ columns }
            dataSource={ this.state.list }
            pagination={ this.state.pagination }
          />
        </div>
        {/* 开通城市弹框 */ }
        <Modal
          title="开通城市"
          visible={ this.state.isShowOpenCity }
          onCancel={ () => {
            this.setState({
              isShowOpenCity: false
            })
          } }
          onOk={ this.handleSubmit }
        >
          <OpenCityForm wrappedComponentRef={ (inst: any) => { this.cityForm = inst; } } />
        </Modal>
      </div>
    )
  }
}
