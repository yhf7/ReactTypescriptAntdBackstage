export interface IProps extends Typings.RouteProps<any, any> {
  form?:any
}

export interface IState {
  list: object[],
  isShowOpenCity: boolean, // 弹框显示状态
  pagination?:any
}

export interface IParams {
  page?: number
}

// export interface IFormList {
  
// }