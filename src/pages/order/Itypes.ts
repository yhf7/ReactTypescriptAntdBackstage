export interface IProps extends Typings.RouteProps<any, any> {
  // form?:any
}

export interface IState {
  list?: object[],
  orderInfo: any,// 确认订单结束数据
  orderConfirmVisble: boolean, // 结束订单弹框显示隐藏开关
  selectedItem: object,
  selectedRowKeys?: any
  pagination?:any
  selectedIds?:any
}

export interface IParams {
  page?: number
}