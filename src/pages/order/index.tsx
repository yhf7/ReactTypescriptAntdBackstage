/**
 * 城市管理
 * 头部调用公共控件组件
 * 城市信息表格，以及开通城市
 * 开通城市： modal弹框
 */
import API from "@/API";
import BaseForm from '@/components/BaseForm'
import ETable from '@/components/ETable'
import { Button, Card, Form, message, Modal } from 'antd'
import React, { Component } from 'react'
import { IParams, IProps, IState } from "./Itypes";

const FormItem = Form.Item;

export default class Order extends Component<IProps, IState> {

  public state: IState
  public params: IParams
  public formList: object[];
  constructor(props: Readonly<IProps>) {
    super(props);
    // 初始化state数据
    this.state = {
      list: [],// 表格数据
      orderInfo: [],// 确认订单结束数据
      orderConfirmVisble: false, // 结束订单弹框显示隐藏开关
      selectedItem: {}
    }
    // 自定义params数据，请求时默认数据
    this.params = {
      page: 1
    }
    // 组件数据
    this.formList = [
      {
        type: 'SELECT',
        label: '城市',
        field: 'city',
        placeholder: '全部',
        initialValue: '1',
        width: 80,
        list: [{ 'id': "0", name: '全部' }, { 'id': "1", name: '广东' }, { 'id': "2", name: '广州' }, { 'id': "3", name: '深圳' }]
      },
      {
        type: 'INPUT',
        label: '模式',
        field: 'mode',
        placeholder: '请输入模式',
        width: 100,
        id: 'input'
      },
      {
        'type': '时间查询',
        field: 'order_time',
      },
      {
        type: 'SELECT',
        field: 'order_status',
        label: '订单状态',
        placeholder: '全部',
        initialValue: '1',
        width: 100,
        list: [{ id: '0', name: '全部' }, { id: '1', name: '进行中' }, { id: '2', name: '结束行程' }]
      },
    ]
  }

  /**
   * 页面执行完dom后执行的函数
   */
  public componentDidMount() {
    // 默认调用api获取表格数据
    this.requestList()
  }

  // 默认请求我们的接口数据
  public requestList = () => {
    // 调用公共封装的列表请求函数
    API.getOrderList(this, this.params, true)
  }

  /**
   * 点击结束订单执行的函数
   */
  public handleConfirm = () => {
    const item: { id?: number } = this.state.selectedItem;// 单选记录的表格数据
    // 判断是否选择了，没有选择就弹框提示阻止运行
    if (Object.keys(item).length < 1) {
      Modal.info({
        title: '信息',
        content: '请选择一条订单进行结束'
      })
      return;
    }
    // 如果选择了请求api传入选择的数据id
    API.getOrderEbike({
      data: {
        params: {
          orderId: item.id
        }
      },
      isMock: true
    }).then((res: any) => {
      // 返回数据判断是否成功，记录数据并开启弹框显示结束数据确认弹框
      if (res.code == 0) {
        this.setState({
          orderInfo: res.result,
          orderConfirmVisble: true
        })
      }
    })
  }

  /**
   * 确定结束订单执行的函数
   */
  public handleFinishOrder = () => {
    // 选中表格的数据
    const item: { id?: number } = this.state.selectedItem;
    // 请求回调
    API.getOrderFinish({
      data: {
        params: {
          orderId: item.id
        }
      },
      isMock: true
    }).then((res: any) => {
      if (res.code == 0) {
        message.success('订单结束成功')
        this.setState({
          orderConfirmVisble: false
        })
        this.requestList();
      }
    })
  }

  /**
   * 点击订单详情
   */
  public openOrderDetail = () => {
    const item: { id?: number } = this.state.selectedItem;// 单选记录的表格数据
    // 判断是否选择了，没有选择就弹框提示阻止运行
    if (Object.keys(item).length < 1) {
      Modal.info({
        title: '信息',
        content: '请先选择一条订单'
      })
      return;
    }
    window.open(`/common/order/detail/${item.id}`,'_blank')
  }

  // 子组件调用方法
  /**
   * 子组件点击查询
   */
  public handleFilter = (params: object) => {
    this.params = params
    this.requestList()
  }

  public updateSelectedItem = (selectedRowKeys: any, selectedRows: any, selectedIds?: any) => {
    if (selectedIds) {
      this.setState({
        selectedRowKeys,
        selectedIds,
        selectedItem: selectedRows
      })
    } else {
      this.setState({
        selectedRowKeys,
        selectedItem: selectedRows
      })
    }
  }

  public render() {

    /** 表格数据 */
    const columns = [
      {
        title: '订单编号',
        dataIndex: 'order_sn'
      },
      {
        title: '车辆编号',
        dataIndex: 'bike_sn'
      },
      {
        title: '用户名',
        dataIndex: 'user_name'
      },
      {
        title: '手机号',
        dataIndex: 'mobile'
      },
      {
        title: '里程',
        dataIndex: 'distance',
        render(distance: number) {
          return distance / 1000 + 'Km';
        }
      },
      {
        title: '行驶时长',
        dataIndex: 'total_time'
      },
      {
        title: '状态',
        dataIndex: 'status'
      },
      {
        title: '开始时间',
        dataIndex: 'start_time'
      },
      {
        title: '结束时间',
        dataIndex: 'end_time'
      },
      {
        title: '订单金额',
        dataIndex: 'total_fee'
      },
      {
        title: '实付金额',
        dataIndex: 'user_pay'
      }
    ]

    // 结束订单，弹框表单样式配比
    const formItemLayout = {
      labelCol: { span: 5 },
      wrapperCol: { span: 19 }
    }

    return (
      <div>
        {/* 头部城市 */ }
        <Card>
          <BaseForm filterSubmit={ this.handleFilter } formList={ this.formList } />
        </Card>
        {/* 中间按钮操作 */ }
        <Card style={ { marginTop: 10 } }>
          <Button type="primary" style={ { marginRight: 10 } } onClick={ this.openOrderDetail }>订单详情</Button>
          <Button type="primary" onClick={ this.handleConfirm }>结束订单</Button>
        </Card>
        {/* 底部表格 */ }
        <div className="content-wrap">
          <ETable
            columns={ columns }
            updateSelectedItem={ this.updateSelectedItem }
            selectedRowKeys={ this.state.selectedRowKeys }
            dataSource={ this.state.list }
            pagination={ this.state.pagination }
          />
        </div>
        <Modal
          title="结束订单"
          visible={ this.state.orderConfirmVisble }
          onCancel={ () => {
            this.setState({
              orderConfirmVisble: false
            })
          } }
          onOk={ this.handleFinishOrder }
          width={ 600 }
        >
          <Form layout="horizontal" { ...formItemLayout }>
            <FormItem label="车辆编号">
              { this.state.orderInfo.bike_sn }
            </FormItem>
            <FormItem label="剩余电量">
              { this.state.orderInfo.battery + '%' }
            </FormItem>
            <FormItem label="行程开始时间">
              { this.state.orderInfo.start_time }
            </FormItem>
            <FormItem label="当前位置">
              { this.state.orderInfo.location }
            </FormItem>
          </Form>
        </Modal>
      </div>
    )
  }
}
