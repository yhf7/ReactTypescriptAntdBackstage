import { Button, Card, notification } from 'antd'
import React, { Component } from 'react'
import '../ui.less'

enum xxtype {
  success = "success",
  info = "info",
  warning = "warning",
  error = "error"
}

export default class Notification extends Component {
  public render() {
    const openNotificationWithIcon = (type: xxtype, plac:any = 'topRight') => {
      notification[type]({
        message: '发工资',
        placement: plac,
        description:
          '上个月考勤22天，迟到12天，实发工资250，请笑纳',
      });
    };

    return (
      <div>
        <Card title="通知提醒框" className="card-wrap">
          <Button type="primary" onClick={ () => openNotificationWithIcon(xxtype.success) }>Success</Button>
          <Button type="primary" onClick={ () => openNotificationWithIcon(xxtype.info) }>Info</Button>
          <Button type="primary" onClick={ () => openNotificationWithIcon(xxtype.warning) }>Warning</Button>
          <Button type="primary" onClick={ () => openNotificationWithIcon(xxtype.error) }>Error</Button>
        </Card>
        <Card title="控制输出位置通知提醒框" className="card-wrap card-top">
          <Button type="primary" onClick={ () => openNotificationWithIcon(xxtype.success, 'topLeft') }>Success</Button>
          <Button type="primary" onClick={ () => openNotificationWithIcon(xxtype.info, 'topRight') }>Info</Button>
          <Button type="primary" onClick={ () => openNotificationWithIcon(xxtype.warning, 'bottomLeft') }>Warning</Button>
          <Button type="primary" onClick={ () => openNotificationWithIcon(xxtype.error, 'bottomRight') }>Error</Button>
        </Card>
      </div>
    )
  }
}