export interface IProps extends Typings.RouteProps<any, any> {
}

export interface IState {
  visible: boolean,
  currentImg: string
}