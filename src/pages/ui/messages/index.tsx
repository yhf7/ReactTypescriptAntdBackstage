import { Button,Card, message } from 'antd'
import React, { Component } from 'react'
import '../ui.less'

enum xxtype {
  success = "success",
  info = "info",
  warning = "warning",
  error = "error",
  loading = "loading"
}

export default class Messages extends Component {
  public render() {
    const openMessages = (type: xxtype) => {
      message[type]('恭喜你，React课程晋级成功');
    };

    return (
      <div>
        <Card title="全局提醒框" className="card-wrap">
          <Button type="primary" onClick={ () => openMessages(xxtype.success) }>Success</Button>
          <Button type="primary" onClick={ () => openMessages(xxtype.info) }>Info</Button>
          <Button type="primary" onClick={ () => openMessages(xxtype.warning) }>Warning</Button>
          <Button type="primary" onClick={ () => openMessages(xxtype.error) }>Error</Button>
          <Button type="primary" onClick={ () => openMessages(xxtype.loading) }>Loading</Button>
        </Card>
      </div>
    )
  }
}