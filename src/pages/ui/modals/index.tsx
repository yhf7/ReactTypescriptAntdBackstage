import { Button, Card, Modal } from 'antd';
import React, { Component } from 'react'
import '../ui.less'
import { IProps, IState } from "./types";

export default class Modals extends Component<IProps, IState>  {

  public state: IState
  constructor(props: Readonly<IProps>) {
    super(props);
    this.state = {
      modalVisible1: false,
      modalVisible2: false,
      modalVisible3: false,
      modalVisible4: false,
      modalArray: [
        {
          title: 'React Open',
          date: '欢迎使用YHF后台管理系统，React Open'
        },
        {
          title: '自定义页脚',
          date: '欢迎使用YHF后台管理系统，自定义页脚',
          okText: "好的",
          cancelText: "算了"
        },
        {
          title: '顶部20px弹框',
          date: '欢迎使用YHF后台管理系统，顶部20px弹框',
          style: { top: 20 }
        },
        {
          title: '水平垂直居中',
          date: '欢迎使用YHF后台管理系统，水平垂直居中',
          centered: true
        },
      ]
    };
  }

  public setModalVisible(modalVisible: string,e=false) {
    this.setState({ [modalVisible]: e });
  }

  public render() {
    const { confirm } = Modal;

    function showConfirm() {
      confirm({
        title: '确定?',
        content: '你确定你学会React了吗？',
        onOk() {
          return new Promise((resolve, reject) => {
            setTimeout(Math.random() > 0.5 ? resolve : reject, 1000);
          }).catch(() => console.log('Oops errors!'));
        }
      });
    }

    function info() {
      Modal.info({
        title: 'This is a notification message',
        content: (
          <div>
            <p>some messages...some messages...</p>
            <p>你确定你学会React了吗？</p>
          </div>
        ),
        // tslint:disable-next-line:no-empty
        onOk() { },
      });
    }

    function success() {
      Modal.success({
        title: 'This is a success message',
        content: '你确定你学会React了吗？',
      });
    }

    function error() {
      Modal.error({
        title: 'This is an error message',
        content: '你确定你学会React了吗？',
      });
    }

    function warning() {
      Modal.warning({
        title: 'This is a warning message',
        content: '你确定你学会React了吗？',
      });
    }

    return (
      <div>
        <Card title="基础模态框" className="card-wrap">
          <Button type="primary" onClick={ () => this.setModalVisible('modalVisible1',true) }>
            Open
          </Button>
          <Button type="primary" onClick={ () => this.setModalVisible('modalVisible2',true) }>
            自定义页脚
          </Button>
          <Button type="primary" onClick={ () => this.setModalVisible('modalVisible3',true) }>
            顶部20px弹框
          </Button>
          <Button type="primary" onClick={ () => this.setModalVisible('modalVisible4',true) }>
            水平垂直居中
          </Button>
          { this.state.modalArray.map((item, i) => (
            <Modal
              title={ item.title }
              visible={ this.state['modalVisible'+(i+1)] }
              onOk={ () => this.setModalVisible('modalVisible'+(i+1))}
              onCancel={ () => this.setModalVisible('modalVisible'+(i+1)) }
              key={ i }
              okText={ item.okText ? item.okText : null }
              cancelText={ item.cancelText ? item.cancelText : null }
              style={ item.style }
              centered={ item.centered ? item.centered : false }
            >
              <p>{ item.date }</p>
            </Modal>
          )) }
        </Card>
        <Card title="信息确认框" className="card-wrap card-top">
          <Button type="primary" onClick={ showConfirm }>Confirm</Button>
          <Button type="primary" onClick={ info }>Info</Button>
          <Button type="primary" onClick={ success }>Success</Button>
          <Button type="primary" onClick={ error }>Error</Button>
          <Button type="primary" onClick={ warning }>Warning</Button>
        </Card>
      </div>
    )
  }
}