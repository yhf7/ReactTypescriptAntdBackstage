import { Card, Icon , message, Tabs} from 'antd'
import React, { Component } from 'react'
import '../ui.less'
import { IProps, IState } from "./types";

export default class Tab extends Component<IProps, IState> {

  public state: IState
  public newTabIndex: number;
  constructor(props: Readonly<IProps>) {
    super(props);
    this.newTabIndex = 0;
    const panes = [
      { title: 'Tab 1', content: 'Content of Tab 1', key: '1' },
      { title: 'Tab 2', content: 'Content of Tab 2', key: '2' },
      {
        title: 'Tab 3',
        content: 'Content of Tab 3',
        key: '3',
        closable: false,
      },
    ];
    this.state = {
      activeKey: panes[0].key,
      panes,
    };
  }

  public onChange = (activeKey:any) => {
    this.setState({ activeKey });
  };

  public onEdit = (targetKey:any, action:string) => {
    (this as any)[action](targetKey);
  };

  public add = () => {
    const { panes } = this.state;
    const activeKey = `newTab${this.newTabIndex++}`;
    panes.push({ title: 'New Tab', content: 'Content of new Tab', key: activeKey });
    this.setState({ panes, activeKey });
  };

  public remove = (targetKey:string) => {
    let { activeKey } = this.state;
    let lastIndex:number=0;
    this.state.panes.forEach((pane:any, i:number) => {
      if (pane.key === targetKey) {
        lastIndex = i - 1;
      }
    });
    const panes = this.state.panes.filter((pane:any) => pane.key !== targetKey);
    if (panes.length && activeKey === targetKey) {
      if (lastIndex >= 0) {
        activeKey = panes[lastIndex].key;
      } else {
        activeKey = panes[0].key;
      }
    }
    this.setState({ panes, activeKey });
  };

  public render() {
    const { TabPane } = Tabs;

    function callback(key:any) {
      message.info('Hi,您选择了页签' + key);
    }

    return (
      <div>
        <Card title="Tab页签" className="card-wrap">
          <Tabs defaultActiveKey="1" onChange={ callback }>
            <TabPane tab="Tab 1" key="1">
              欢迎学习React课程
            </TabPane>
            <TabPane tab="Tab 2" disabled={true} key="2">
              欢迎学习React课程
            </TabPane>
            <TabPane tab="Tab 3" key="3">
              React是一个非常受欢迎的MV*框架
            </TabPane>
          </Tabs>
        </Card>
        <Card title="Tab页签" className="card-wrap card-top">
          <Tabs defaultActiveKey="1" onChange={ callback }>
            <TabPane tab={
              <span>
                <Icon type="plus" />
                Tab 1
              </span>
            } key="1">
              欢迎学习React课程
            </TabPane>
            <TabPane tab={
              <span>
                <Icon type="edit" />
                Tab 2
              </span>
            } key="2">
              
              欢迎学习React课程
            </TabPane>
            <TabPane tab={
              <span>
                <Icon type="delete" />
                Tab 3
              </span>
            } key="3">
              
              React是一个非常受欢迎的MV*框架
            </TabPane>
          </Tabs>
        </Card>
        <Card title="Tab页签" className="card-wrap card-top">
          <Tabs
            onChange={ this.onChange }
            activeKey={ this.state.activeKey }
            type="editable-card"
            onEdit={ this.onEdit }
          >
            { this.state.panes.map((pane:any) => (
              <TabPane tab={ pane.title } key={ pane.key } closable={ pane.closable }>
                { pane.content }
              </TabPane>
            )) }
          </Tabs>
        </Card>
      </div>
    )
  }
}