export interface IProps extends Typings.RouteProps<any, any> {
}

export interface IState {
  activeKey: string,
  panes?: any
}