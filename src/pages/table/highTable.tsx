/**
 * 高级表格
 * 1. 固定表格头部内容滚动
 * 2. 左右侧固定中间滚动
 * 3. 表格内部列排序
 * 4. 添加操作按钮
 */
import API from '@/API'
import { Badge, Button, Card, message, Modal, Table } from 'antd'
import React, { Component } from 'react'
import { IParams, IProps, IState } from "./types2";

export default class BasicTable extends Component<IProps, IState, IParams>  {

  public state: IState
  public params: IParams
  constructor(props: Readonly<IProps>) {
    super(props);
    this.state = {
      // dataSource: {}
    };
    // 自定义请求初始化数据
    this.params = {
      page: 1 // 分页树
    }
  }

  // dom执行完跑
  public componentDidMount() {
    // 请求列表数据
    this.request()
  }

  // 动态获取mock数据
  public request = () => {
    // let _this = this;
    API.getTableList({
      data: {
        params: {
          page: this.params.page
        }
      },
      isMock: true
    }).then((res:any) => {
      if (res.code === 0) {
        // 循环加key
        res.result.list.map((item:any, i:number) => {
          return item.key = i
        })
        // 设置列表数据
        this.setState({
          dataSource: res.result.list,
        })
      }

    })
  }

  /**
   * 表格内部排序点击事件
   */
  public onChange = (pagination:any, filters:any, sorter:any) => {
    console.log('params', pagination, filters, sorter);
  }

  // 删除对象
  public handleDelete = (item:any) => {
    Modal.confirm({
      title: '提示',
      content: `您确定删除这些数据吗?`,
      onOk: () => {
        message.success('删除成功')
      }
    })
  }

  public render() {
    // 定制表头
    const columns = [
      {
        title: 'id',
        dataIndex: 'id',
        width: 150,
      },
      {
        title: '用户名',
        dataIndex: 'username',
        width: 150,
      },
      {
        title: '性别',
        dataIndex: 'sex',
        width: 150,
        render(sex: number) {
          return sex === 1 ? '男' : '女'
        }
      },
      {
        title: '状态',
        dataIndex: 'state',
        width: 150,
        render(state:number) {
          enum config {
            '咸鱼一条',
            '风华浪子',
            '北大才子',
            '百度FE',
            '创业者'
          }
          return config[state];
        }
      },
      {
        title: '爱好',
        dataIndex: 'interest',
        width: 150,
        render(abc:number) {
          enum config {
            '游泳',
            '打篮球',
            '踢足球',
            '跑步',
            '爬山',
            '骑行',
            '桌球',
            '麦霸'
          }
          return config[abc];
        }
      },
      {
        title: '生日',
        dataIndex: 'birthday',
        width: 150,
      },
      {
        title: '地址',
        dataIndex: 'address',
        width: 150,
      },
      {
        title: '早起时间',
        dataIndex: 'time',
        width: 150,
      }
    ];

    // 定制表头2
    const columns2 = [
      {
        title: 'id',
        dataIndex: 'id',
        width: 150,
        fixed: true,
      },
      {
        title: '用户名',
        dataIndex: 'username',
        width: 150,
        fixed: true,
      },
      {
        title: '性别',
        dataIndex: 'sex',
        width: 150,
        render(sex:number) {
          return sex === 1 ? '男' : '女'
        }
      },
      {
        title: '状态',
        dataIndex: 'state',
        width: 150,
        render(state:number) {
          enum config {
            '咸鱼一条',
            '风华浪子',
            '北大才子',
            '百度FE',
            '创业者'
          }
          return config[state];
        }
      },
      {
        title: '爱好',
        dataIndex: 'interest',
        width: 150,
        render(abc:number) {
          enum config {
            '游泳',
            '打篮球',
            '踢足球',
            '跑步',
            '爬山',
            '骑行',
            '桌球',
            '麦霸'
          }
          return config[abc];
        }
      },
      {
        title: '生日',
        dataIndex: 'birthday',
        width: 150,
      },
      {
        title: '生日',
        dataIndex: 'birthday',
        width: 150,
      },
      {
        title: '生日',
        dataIndex: 'birthday',
        width: 150,
      },
      {
        title: '生日',
        dataIndex: 'birthday',
        width: 150,
      },
      {
        title: '生日',
        dataIndex: 'birthday',
        width: 150,
      },
      {
        title: '生日',
        dataIndex: 'birthday',
        width: 150,
      },
      {
        title: '生日',
        dataIndex: 'birthday',
        width: 150,
      },
      {
        title: '生日',
        dataIndex: 'birthday',
        width: 150,
      },
      {
        title: '生日',
        dataIndex: 'birthday',
        width: 150,
      },
      {
        title: '生日',
        dataIndex: 'birthday',
        width: 150,
      },
      {
        title: '地址',
        dataIndex: 'address',
        width: 150,
        fixed: "right" as any,
      },
      {
        title: '早起时间',
        dataIndex: 'time',
        width: 150,
        fixed: "right" as any,
      }
    ];
    // 定制表头3
    const columns3 = [
      {
        title: 'id',
        dataIndex: 'id',
      },
      {
        title: '用户名',
        dataIndex: 'username',
      },
      {
        title: '性别',
        dataIndex: 'sex',
        render(sex:number) {
          return sex === 1 ? '男' : '女'
        }
      },
      {
        title: '年龄',
        dataIndex: 'age',
        width: 90,
        sorter: (a:any, b:any) => a.age - b.age,
      },
      {
        title: '状态',
        dataIndex: 'state',
        render(state:number) {
          enum config {
            "咸鱼一条",
            "风华浪子",
            "北大才子",
            "百度FE",
            "创业者"
          }
          return config[state];
        }
      },
      {
        title: '爱好',
        dataIndex: 'interest',
        render(abc:number) {
          enum config {
            '游泳',
            '打篮球',
            '踢足球',
            '跑步',
            '爬山',
            '骑行',
            '桌球',
            '麦霸'
          }
          return config[abc];
        }
      },
      {
        title: '生日',
        dataIndex: 'birthday',
      },
      {
        title: '地址',
        dataIndex: 'address',
      },
      {
        title: '早起时间',
        dataIndex: 'time',
      }
    ];
    // 定制表头4
    const columns4 = [
      {
        title: 'id',
        dataIndex: 'id',
      },
      {
        title: '用户名',
        dataIndex: 'username',
      },
      {
        title: '性别',
        dataIndex: 'sex',
        render(sex:number) {
          return sex === 1 ? '男' : '女'
        }
      },
      {
        title: '年龄',
        dataIndex: 'age',
        width: 90
      },
      {
        title: '状态',
        dataIndex: 'state',
        render(state:number) {
          enum config {
            'success',
            'error',
            'default',
            'processing',
            'warning'
          }
          enum config1 {
            '咸鱼一条',
            '风华浪子',
            '北大才子',
            '百度FE',
            '创业者'
          }

          return <Badge status={ config[state] as any } text={ config1[state] } />;
        }
      },
      {
        title: '爱好',
        dataIndex: 'interest',
        render(abc:any) {
          enum config {
            '游泳',
            '打篮球',
            '踢足球',
            '跑步',
            '爬山',
            '骑行',
            '桌球',
            '麦霸'
          }
          return config[abc];
        }
      },
      {
        title: '生日',
        dataIndex: 'birthday',
      },
      {
        title: '地址',
        dataIndex: 'address',
      },
      {
        title: '操作',
        render: () => {
          return <Button onClick={ (item:any) => { this.handleDelete(item) } }>删除</Button>
        }
      }
    ];

    return (
      <div>
        <Card title="头部固定">
          <Table columns={ columns } dataSource={ this.state.dataSource } bordered={ true } pagination={ false } scroll={ { y: 240 } } />
        </Card>
        <Card title="左侧固定" style={ { margin: '10px 0' } }>
          <Table columns={ columns2 } dataSource={ this.state.dataSource } bordered={ true } pagination={ false } scroll={ { x: 2500, y: 300 } } />
        </Card>
        <Card title="表格排序" style={ { margin: '10px 0' } }>
          <Table columns={ columns3 } dataSource={ this.state.dataSource } bordered={ true } pagination={ false } onChange={ this.onChange } />
        </Card>
        <Card title="操作按钮" style={ { margin: '10px 0' } }>
          <Table columns={ columns4 } dataSource={ this.state.dataSource } bordered={ true } pagination={ false } />
        </Card>
      </div>
    )
  }
}