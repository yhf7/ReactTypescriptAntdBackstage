/**
 * 基础表格页面
 * 基础表格，动态渲染数据，添加单选，添加多选，添加分页
 */
import API from '@/API'
import Utils from '@/utils/utils'
import { Button, Card, message, Modal, Table } from 'antd'
import React, { Component } from 'react'
import { IParams, IProps, IState } from "./types";

export default class BasicTable extends Component<IProps, IState, IParams> {

  public state: IState
  public params: IParams
  constructor(props: Readonly<IProps>) {
    super(props);
    this.state = {
      selectedRowKeys: []
    };
    // 自定义请求初始化数据
    this.params = {
      page: 1 // 分页树
    }
  }

  public componentDidMount() {
    const dataSource = [
      {
        key: '1',
        id: '0',
        username: 'YHF',
        sex: '20',
        state: '1',
        interest: '1',
        birthday: '2000-01-01',
        address: '广东东莞厚街',
        time: '09:00'
      },
      {
        key: '2',
        id: '1',
        username: 'YHF2',
        sex: '20',
        state: '1',
        interest: '1',
        birthday: '2000-01-01',
        address: '广东东莞厚街',
        time: '09:00'
      },
      {
        key: '3',
        id: '2',
        username: 'YHF3',
        sex: '20',
        state: '1',
        interest: '1',
        birthday: '2000-01-01',
        address: '广东东莞厚街',
        time: '09:00'
      },
    ]
    this.setState({
      dataSource
    })
    this.request()
  }

  // 动态获取mock数据
  public request = () => {
    const This = this;
    API.getTableList({
      data: {
        params: {
          page: this.params.page
        }
      },
      isMock: true
    }).then((res: any) => {

      this.setState({
        dataSource2: res.result.list,
        selectedRowKeys: [],
        selectedRows: null,
        pagination: Utils.pagination(res, (current: any) => {
          This.params.page = current;
          This.request()
        })
      })
    })
  }

  public onRowClick = (rec: object, i: number) => {
    const selectKey: number[] = [i]
    this.setState({
      selectedRowKeys: selectKey,
      selectedItem: rec
    })
  }

  public onCheckClick = (rec:object,i:number) => {
    this.setState({
      selectedItem: rec,
      selectedRowKeys: [i,...this.state.selectedRowKeys] as number[]
    })
  }

  public onSelectChange = (selectedRowKeys: string[] | number[]) => {
    console.log('selectedRowKeys changed: ', selectedRowKeys);
    this.setState({ selectedRowKeys });
  };

  // 多选删除操作
  public handleDelete = () => {
    const rows = this.state.selectedRows;
    const ids: any[] = []
    if (!rows) {
      return false
    }
    rows.map((item: any) => {
      return ids.push(item.id)
    })

    Modal.confirm({
      title: '提示',
      content: `您确定删除这些数据吗?${ids}`,
      onOk: () => {
        message.success('删除成功')
        this.request();
      }
    })
  }

  public render() {
    // 定制表头
    const columns = [
      {
        title: 'id',
        dataIndex: 'id',
      },
      {
        title: '用户名',
        dataIndex: 'username'
      },
      {
        title: '性别',
        dataIndex: 'sex',
        render(sex: number) {
          return sex === 1 ? '男' : '女'
        }
      },
      {
        title: '状态',
        dataIndex: 'state',
        render(state: number) {
          enum config {
            '咸鱼一条',
            '风华浪子',
            '北大才子',
            '百度FE',
            '创业者'
          }
          return config[state];
        }
      },
      {
        title: '爱好',
        dataIndex: 'interest',
        render(abc: number) {
          enum config {
            '游泳',
            '打篮球',
            '踢足球',
            '跑步',
            '爬山',
            '骑行',
            '桌球',
            '麦霸'
          }
          return config[abc];
        }
      },
      {
        title: '生日',
        dataIndex: 'birthday'
      },
      {
        title: '地址',
        dataIndex: 'address'
      },
      {
        title: '早起时间',
        dataIndex: 'time'
      }
    ];

    const { selectedRowKeys } = this.state;

    const rowSelection = {
      type: 'radio' as any,
      selectedRowKeys,
      onChange: (selectedRows: any) => {
        this.setState({
          selectedRowKeys,
          selectedItem: selectedRows
        })
        console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
      },
      getCheckboxProps: (record: { name: string, }) => ({
        disabled: record.name === 'Disabled User', // Column configuration not to be checked
        name: record.name,
      }),
    };

    const rowCheckSelection = {
      // type: 'checkbox' as any,
      selectedRowKeys,
      onChange: this.onSelectChange,
      getCheckboxProps: (record: any) => ({
        disabled: record.name === 'Disabled User', // Column configuration not to be checked
        name: record.name,
      }),
    };

    return (
      <div>
        <Card title="基础表格">
          <Table columns={ columns } dataSource={ this.state.dataSource } bordered={ true } pagination={ false } />
        </Card>
        <Card title="动态数据渲染" style={ { margin: '10px 0' } }>
          <Table columns={ columns } dataSource={ this.state.dataSource2 } bordered={ true } pagination={ false } />
        </Card>
        <Card title="Mock-单选" style={ { margin: '10px 0' } }>
          <Table onRow={ (record, index) => {
            return {
              onClick: () => {
                this.onRowClick(record, index)
              },
            };
          } } rowSelection={ rowSelection } columns={ columns } dataSource={ this.state.dataSource2 } bordered={ true } pagination={ false } />
        </Card>
        <Card title="Mock-多选" style={ { margin: '10px 0' } }>
          <div style={ { marginBottom: 10 } }>
            <Button onClick={ this.handleDelete }>删除</Button>
          </div>
          <Table onRow={ (record, index) => {
            return {
              onClick: () => {
                this.onCheckClick(record, index)
              },
            };
          } } rowSelection={ rowCheckSelection } columns={ columns } dataSource={ this.state.dataSource2 } bordered={ true } pagination={ false } />
        </Card>
        <Card title="Mock-表格分页" style={ { margin: '10px 0' } }>
          <Table columns={ columns } dataSource={ this.state.dataSource2 } bordered={ true } pagination={ this.state.pagination } />
        </Card>
      </div>
    )
  }
}