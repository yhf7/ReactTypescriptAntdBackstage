export interface IProps extends Typings.RouteProps<any, any> {
  form?: any
}

export interface IState {
  dataSource?: Array<{ key: string, id: string, username: string, sex: string, state: string, interest: string, birthday: string, address: string, time: string }>
  dataSource2?: any[]
  selectedRowKeys: string[] | number[]
  selectedRows?: number[]|null
  pagination?: any
  selectedItem?: any
}

export interface IParams {
  page: number
}