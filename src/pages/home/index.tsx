/**
 * 主页文件
 * 展示欢迎界面
 */
import React, { Component } from 'react';
import './index.less'

export default class Home extends Component {
  public render() {
    return (
      <div className="home-wrap">
        欢迎使用React + Typescript + antd 搭建的后台管理系统
      </div>
    )
  }
}