/**
 * 表单登录页
 * 1. 第一个行内表单未实现效果
 * 2. 第二个登录带验证的表单，需要输入两个input才可以登录，并且name还有长度限制
 */
import { Button, Card, Checkbox, Form, Icon, Input, message } from 'antd'
import React, { Component } from 'react'
import '../form.less'
import { IProps } from "./types";

@(Form.create() as any)
export default class FormLogin extends Component<IProps, {}>  {

  /**
   * 点击登录时执行的函
   * @param e 表单属性
   */
  public handleSubmit = (e:any) => {
    e.preventDefault();  // 阻止页面刷新
    // 获取表单数据
    this.props.form.validateFields((err:any, values:{password:string}) => {
      if (!err) {
        console.log('Received values of form: ', values);
        message.success('恭喜您登录成功,密码为:' + values.password);
      }
    });
  };

  public render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <div>
        {/* 第一个表单 */ }
        <Card title="登陆行内表单">
          <Form layout="inline">
            <Form.Item >
              <Input
                placeholder="请输入用户名"
              />
            </Form.Item>
            <Form.Item >
              <Input
                placeholder="请输入密码"
              />
            </Form.Item>
            <Form.Item >
              <Button type="primary">登录</Button>
            </Form.Item>
          </Form>
        </Card>
        {/* 第二个表单 */ }
        <Card title="登录水平表单" className="card-top">
          <Form onSubmit={ this.handleSubmit } className="login-form">
            {/* username组件，rules为验证规则 */ }
            <Form.Item>
              { getFieldDecorator('username', {
                initialValue: '',
                rules: [
                  {
                    required: true,
                    message: '用户名不能为空'
                  },
                  {
                    min: 5, max: 10,
                    message: '长度不在范围内'
                  },
                  {
                    pattern: new RegExp('^\\w+$', 'g'),
                    message: '用户名必须为字母或者数字'
                  }
                ],
              })(
                <Input
                  prefix={ <Icon type="user" style={ { color: 'rgba(0,0,0,.25)' } } /> }
                  placeholder="请输入用户名"
                />,
              ) }
            </Form.Item>
            <Form.Item>
              { getFieldDecorator('password', {
                initialValue: '',
                rules: [{ required: true, message: 'Please input your Password!' }],
              })(
                <Input
                  prefix={ <Icon type="lock" style={ { color: 'rgba(0,0,0,.25)' } } /> }
                  type="password"
                  placeholder="请输入密码"
                />,
              ) }
            </Form.Item>
            <Form.Item>
              { getFieldDecorator('remember', {
                valuePropName: 'checked',
                initialValue: true,
              })(<Checkbox>记住密码</Checkbox>) }
              <a className="login-form-forgot" href="https://baidu.com">
                忘记密码
              </a>
              <Button type="primary" htmlType="submit" className="login-form-button">
                登录
              </Button>
            </Form.Item>
          </Form>
        </Card>
      </div>
    )
  }
}

// export default Form.create()(FormLogin);