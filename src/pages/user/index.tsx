import API from '@/API'
import BaseForm from '@/components/BaseForm'
import ETable from '@/components/ETable'
import UserForm from '@/components/UserForm'
import { Button, Card, Modal } from 'antd'
import React, { Component } from 'react'
import { IParams, IProps, IState } from "./types";

export default class User extends Component<IProps, IState, IParams> {

  public state: IState
  public params: IParams
  public formList: any[]
  public userForm:any;
  constructor(props: Readonly<IProps>) {
    super(props);
    this.state = {
      selectedItem: {},
    };
    // 自定义请求初始化数据
    this.params = {
      page: 1 // 分页树
    }
    // 组件数据
    this.formList = [
      {
        type: 'INPUT',
        label: '用户名',
        field: 'user_name',
        placeholder: '请输入名称',
        width: 100
      },
      {
        type: 'INPUT',
        label: '手机号',
        field: 'user_mobile',
        placeholder: '请输入手机号',
        width: 120,
      },
      {
        type: 'DATE',
        label: '请选择入职日期',
        field: 'user_date',
        placeholder: '请输入日期',
        width: 100,
      },
    ]
  }

  // 执行完dom操作后运行
  public componentDidMount() {
    // 默认调用请求数据
    this.requestList();
  }

  // 默认请求我们的接口数据
  public requestList = () => {
    // 调用公共封装的列表请求函数
    API.getUserList(this, this.params, true)
  }

  /** 
   * 员工操作函数
   * @param {String} type 输入操作的类型
   */
  public hanleOperate = (type:any) => {
    const item = this.state.selectedItem;
    if (type == 'create') {
      this.setState({
        type,
        isVisible: true,
        title: '创建员工'
      })
    } else {
      if (!item) {
        Modal.info({
          title: '信息',
          content: '请选择一个用户'
        })
        return;
      }
      if (type == 'edit' || type == 'detail') {
        this.setState({
          type,
          isVisible: true,
          title: type == 'edit' ? '编辑用户' : '查看详情',
          userInfo: item
        })
      } else if (type == 'delete') {
        Modal.confirm({
          title: '确定要删除此用户吗？',
          onOk: () => {
            API.getUserDelete({

              data: {
                params: {
                  id: item.id
                }
              },
              isMock: true
            }).then((res:any) => {
              if (res.code == 0) {
                this.setState({
                  isVisible: false
                })
                this.requestList();
              }
            })
          }
        })
      }
    }
  }

  // 员工创建提交
  public handleSubmit = () => {
    this.userForm.props.form.validateFields((err:any) => {
      if (!err) {
        const type = this.state.type == 'create';
        const data = this.userForm.props.form.getFieldsValue();
        if (type) {
          API.getUserAdd({
            data: {
              params: data
            },
            isMock: true
          }).then((res:any) => {
            if (res.code == 0) {
              this.setState({
                isVisible: false
              })
              this.userForm.props.form.resetFields()
              this.requestList()
            }
          })
        } else {
          API.getUserEdit({
            data: {
              params: data
            },
            isMock: true
          }).then((res:any) => {
            if (res.code == 0) {
              this.setState({
                isVisible: false
              })
              this.userForm.props.form.resetFields()
              this.requestList()
            }
          })
        }
        
      }
    });
  }

  public updateSelectedItem = (selectedRowKeys: any, selectedRows: any, selectedIds?: any) => {
    if (selectedIds) {
      this.setState({
        selectedRowKeys,
        selectedIds,
        selectedItem: selectedRows
      })
    } else {
      this.setState({
        selectedRowKeys,
        selectedItem: selectedRows
      })
    }
  }

  // 子组件调用方法
  /**
   * 子组件点击查询
   */
  public handleFilter = (params: object) => {
    this.params = params
    this.requestList()
  }

  public render() {
    // 表格模型
    const columns = [{
      title: 'id',
      dataIndex: 'id'
    }, {
      title: '用户名',
      dataIndex: 'username'
    }, {
      title: '性别',
      dataIndex: 'sex',
      render(sex:number) {
        return sex == 1 ? '男' : '女'
      }
    }, {
      title: '状态',
      dataIndex: 'state',
      render(state:number) {
        enum config {
          '咸鱼一条',
          '风华浪子',
          '北大才子一枚',
          '百度FE',
          '创业者'
        }
        return config[state];
      }
    }, {
      title: '爱好',
      dataIndex: 'interest',
      render(interest:number) {
        enum config {
          '游泳',
          '打篮球',
          '踢足球',
          '跑步',
          '爬山',
          '骑行',
          '桌球',
          '麦霸'
        }
        return config[interest];
      }
    }, {
      title: '爱好',
      dataIndex: 'isMarried',
      render(isMarried:string) {
        return isMarried ? '已婚' : '未婚'
      }
    }, {
      title: '生日',
      dataIndex: 'birthday'
    }, {
      title: '联系地址',
      dataIndex: 'address'
    }, {
      title: '早起时间',
      dataIndex: 'time'
    }
    ];

    let footer = {};
    if (this.state.type == 'detail') {
      footer = {
        footer: null
      }
    }

    return (
      <div>
        <Card>
          <BaseForm filterSubmit={ this.handleFilter } formList={ this.formList } />
        </Card>
        {/* 中间按钮操作 */ }
        <Card style={ { marginTop: 10 } } className="operate-wrap">
          <Button type="primary" style={ { marginRight: 10 } } icon="plus" onClick={ () => this.hanleOperate('create') }>创建员工</Button>
          <Button style={ { marginRight: 10 } } icon="edit" onClick={ () => this.hanleOperate('edit') }>编辑员工</Button>
          <Button style={ { marginRight: 10 } } icon="info-circle" onClick={ () => this.hanleOperate('detail') }>员工详情</Button>
          <Button type="danger" style={ { marginRight: 10 } } icon="delete" onClick={ () => this.hanleOperate('delete') }>删除员工</Button>
        </Card>
        {/* 底部表格 */ }
        <div className="content-wrap">
          <ETable
            columns={ columns }
            updateSelectedItem={ this.updateSelectedItem }
            selectedRowKeys={ this.state.selectedRowKeys }
            dataSource={ this.state.list }
            pagination={ this.state.pagination }
          />
        </div>
        <Modal
          title={ this.state.title }
          visible={ this.state.isVisible }
          onOk={ this.handleSubmit }
          onCancel={ () => {
            this.setState({
              isVisible: false,
              userInfo: {}
            })
            this.userForm.props.form.resetFields()
          } }
          width={ 600 }
          { ...footer }
        >
          <UserForm userInfo={ this.state.userInfo } type={ this.state.type } wrappedComponentRef={ (inst) => this.userForm = inst }/>
        </Modal>
      </div>
    )
  }
}
