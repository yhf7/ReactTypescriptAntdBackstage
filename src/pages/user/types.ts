export interface IProps extends Typings.RouteProps<any, any> {

}

export interface IState {
  selectedItem:any
  title?:string
  type?:string
  isVisible?:boolean
  userInfo?:any
  selectedRowKeys?:any
  list?:any
  pagination?:any
  selectedIds?:any
}

export interface IParams {
  page?: number
}