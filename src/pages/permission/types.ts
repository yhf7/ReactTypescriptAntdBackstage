export interface IProps extends Typings.RouteProps<any, any> {

}

export interface IState {
  isRoleVisible: boolean, // 控制创建用户显示隐藏
  isPermVisible: boolean, // 控制设置权限显示隐藏
  isUserVisible: boolean, // 控制用户授权显示隐藏
  detailInfo: object, // 选中的数据，传入给子组件
  menuInfo: any[], // 权限选中数组
  selectedItem: {id?:any,menus?:any},
  mockData?:any[],
  targetKeys?:any[]
  selectedRowKeys?:any
  selectedIds?:number
  list:any[]
  pagination:any
}

export interface IParams {
  id: number
}