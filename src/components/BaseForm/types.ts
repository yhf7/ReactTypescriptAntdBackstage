export interface IProps extends Typings.RouteProps<any, any> {
  form?: any
  filterSubmit: filterSubmit
  formList?: Array<{ label?: string, field?: string, initialValue?: string, placeholder?: string, width?: string, type?: string, list?: {id:string,name:string} }>
}

type filterSubmit = (fieldsValue:any) => void;
