import Utils from '@/utils/utils';
import { Button, Checkbox, DatePicker, Form, Input, Select } from 'antd'
import React, { Component } from 'react'
import { IProps } from "./types";
const FormItem = Form.Item;


@(Form.create({}) as any)
export default class FilterForm extends Component<IProps, {}> {

    public handleFilterSubmit = () => {
        const fieldsValue = this.props.form.getFieldsValue();
        this.props.filterSubmit(fieldsValue);
    }

    public reset = () => {
        this.props.form.resetFields();
    }

    public initFormList = () => {
        const { getFieldDecorator } = this.props.form;
        const formList = this.props.formList;
        const formItemList: JSX.Element[] = [];
        if (formList && formList.length > 0) {
            formList.forEach((item: { label?: string, field?: string, initialValue?: string, placeholder?: string, width?: string, type?: string, list?: { id: string, name: string } }) => {
                const label = item.label;
                const field = item.field;
                const initialValue = item.initialValue || '';
                const placeholder = item.placeholder;
                const width = item.width;
                if (item.type == '时间查询') {
                    const BeginTime: JSX.Element = <FormItem label="订单时间" key="order_time">
                        {
                            getFieldDecorator('begin_time')(
                                <DatePicker showTime={ true } placeholder={ placeholder } format="YYYY-MM-DD HH:mm:ss" />
                            )
                        }
                    </FormItem>
                    formItemList.push(BeginTime)
                    const EndTime: JSX.Element = <FormItem label="~" colon={ false } key="order_time1">
                        {
                            getFieldDecorator('end_time')(
                                <DatePicker showTime={ true } placeholder={ placeholder } format="YYYY-MM-DD HH:mm:ss" />
                            )
                        }
                    </FormItem>;
                    formItemList.push(EndTime)
                } else if (item.type == 'INPUT') {
                    const INPUT: JSX.Element = <FormItem label={ label } key={ field }>
                        {
                            getFieldDecorator([field], {
                                initialValue
                            })(
                                <Input type="text" placeholder={ placeholder } />
                            )
                        }
                    </FormItem>;
                    formItemList.push(INPUT)
                } else if (item.type == 'SELECT') {
                    const SELECT: JSX.Element = <FormItem label={ label } key={ field }>
                        {
                            getFieldDecorator([field], {
                                initialValue
                            })(
                                <Select
                                    style={ { width } }
                                    placeholder={ placeholder }
                                >
                                    { Utils.getOptionList(item.list) }
                                </Select>
                            )
                        }
                    </FormItem>;
                    formItemList.push(SELECT)
                } else if (item.type == 'CHECKBOX') {
                    const CHECKBOX: JSX.Element = <FormItem label={ label } key={ field }>
                        {
                            getFieldDecorator([field], {
                                valuePropName: 'checked',
                                initialValue // true | false
                            })(
                                <Checkbox>
                                    { label }
                                </Checkbox>
                            )
                        }
                    </FormItem>;
                    formItemList.push(CHECKBOX)
                }
            })
        }
        return formItemList;
    }
    public render() {
        return (
            <Form layout="inline">
                { this.initFormList() }
                <FormItem>
                    <Button type="primary" style={ { margin: '0 20px' } } onClick={ this.handleFilterSubmit }>查询</Button>
                    <Button onClick={ this.reset }>重置</Button>
                </FormItem>
            </Form>
        );
    }
}