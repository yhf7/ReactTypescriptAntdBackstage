/**
 * 开通城市组件
 * 表单
 * select选择
 */
import { Form, Select } from 'antd'
import React, { Component } from 'react';
import { IProps } from "./types";

const { Option } = Select;
const FormItem = Form.Item;

@(Form.create({}) as any)
export default class OpenCityForm extends Component<IProps> {
  public render() {
    const formItemLayout = {
      labelCol: {
        span: 5
      },
      wrapperCol: {
        span: 19
      }
    }
    const { getFieldDecorator } = this.props.form;
    return (
      <Form layout="horizontal"  { ...formItemLayout }>
        <FormItem label="选择城市">
          {
            getFieldDecorator('city_id', {
              initialValue: '1'
            })(
              <Select style={ { width: 100 } }>
                <Option value="">全部</Option>
                <Option value="1">北京市</Option>
                <Option value="2">天津市</Option>
              </Select>
            )
          }
        </FormItem>
        <FormItem label="营运模式">
          {
            getFieldDecorator('op_mode', {
              initialValue: '1'
            })(
              <Select style={ { width: 100 } }>
                <Option value="1">自营</Option>
                <Option value="2">加盟</Option>
              </Select>
            )
          }
        </FormItem>
        <FormItem label="用车模式">
          {
            getFieldDecorator('use_mode', {
              initialValue: '1'
            })(
              <Select style={ { width: 100 } }>
                <Option value="1">指定停车点</Option>
                <Option value="2">禁停区</Option>
              </Select>
            )
          }
        </FormItem>
      </Form>
    );
  }
}