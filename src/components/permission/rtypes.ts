export interface IProps extends Typings.RouteProps<any, any> {
  form?: any
  wrappedComponentRef?: (fieldsValue: any) => void
}
