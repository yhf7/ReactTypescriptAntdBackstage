export interface IProps extends Typings.RouteProps<any, any> {
  form?: any
  patchUserInfo: patchUserInfo
  detailInfo:any
  mockData:any
  targetKeys:any
  handleSearch?:any
  wrappedComponentRef?: (fieldsValue: any) => void
}

type patchUserInfo = (targetKeys:any)=>void