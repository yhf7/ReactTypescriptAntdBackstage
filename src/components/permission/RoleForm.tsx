import { Form, Input, Select } from 'antd';
import React,{Component} from 'react';
import { IProps } from "./rtypes";
const FormItem = Form.Item;
const { Option } = Select;

/**
 * 创建角色子组件
 */
@(Form.create({}) as any)
export default class RoleForm extends Component<IProps,{}> {
  public render() {
    const { getFieldDecorator } = this.props.form;

    const formItemLayout = {
      labelCol: { span: 5 },
      wrapperCol: { span: 19 }
    }
    return (
      <Form layout="horizontal" { ...formItemLayout }>
        <FormItem label="角色名称">
          {
            getFieldDecorator('role_name', {
              rules: [
                {
                  required: true,
                  message: '请输入角色名称',
                },
              ],
            })(
              <Input type="text" placeholder="请输入角色名称" />
            )
          }
        </FormItem>
        <FormItem label="状态">
          {
            getFieldDecorator('state', {
              initialValue: 1
            })(
              <Select style={ { width: 120 } }>
                <Option value={ 1 }>开启</Option>
                <Option value={ 0 }>关闭</Option>
              </Select>
            )
          }
        </FormItem>
      </Form>
    )
  }
}