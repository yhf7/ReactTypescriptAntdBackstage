export interface IProps extends Typings.RouteProps<any, any> {
  form?: any
  detailInfo: any
  patchMenuInfo: patchMenuInfo
  menuInfo:any
  wrappedComponentRef?: (fieldsValue: any) => void
}

type patchMenuInfo = (checkedKeys:any) => void
