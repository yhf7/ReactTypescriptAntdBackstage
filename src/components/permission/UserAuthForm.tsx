import { Form, Input, Transfer } from 'antd';
import React, { Component } from 'react';
import { IProps } from "./utypes";
const FormItem = Form.Item;
/**
 * 用户授权子函数
 */
@(Form.create({}) as any)
export default class UserAuthForm extends Component<IProps,{}> {

  /**
   * 数据筛选函数
   */
  public filterOption = (inputValue:any, option:any) => {
    return option.title.indexOf(inputValue) > -1;
  };

  /**
   * 选择时的回调
   * @param {array} targetKey 返回值，右侧返回数据
   * 调用父级保存数据函数传入数据
   */
  public handleChange = (targetKeys:any[]) => {
    this.props.patchUserInfo(targetKeys);
  };

  public render() {
    const detailInfo = this.props.detailInfo;
    const formItemLayout = {
      labelCol: { span: 5 },
      wrapperCol: { span: 18 }
    }
    return (
      <Form layout="horizontal" { ...formItemLayout }>
        <FormItem label="角色名称">
          {

            <Input style={ { width: 120 } } type="text" placeholder={ detailInfo.role_name } disabled={true} />

          }
        </FormItem>
        <FormItem label="选择用户">
          <Transfer
            listStyle={ { width: 200, height: 400 } }
            dataSource={ this.props.mockData }
            showSearch={true}
            titles={ ['待选用户', '已选用户'] }
            filterOption={ this.filterOption }
            targetKeys={ this.props.targetKeys }
            onChange={ this.handleChange }
            render={ item => item.title }
          />
        </FormItem>
      </Form>
    )
  }
}