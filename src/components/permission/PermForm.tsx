// 权限数据
import menuConfig from '@/config/menuConfig';
import { Form, Input, Select, Tree } from 'antd';
import React, { Component } from 'react';
import { IProps } from "./types";

const FormItem = Form.Item;
const { Option } = Select;
const { TreeNode } = Tree

/**
 * 权限控制子组件
 * 表单
 */
@(Form.create({}) as any)
export default class PermForm extends Component<IProps,{}> {

  public renderThreeNodes = (data:any[]) => {
    return data.map((item: { children:any}) => {
      if (item.children) {
        return <TreeNode { ...item }>
          { this.renderThreeNodes(item.children) }
        </TreeNode>
      } else {
        return <TreeNode { ...item } />
      }
    })
  }

  public onCheck = (checkedKeys:any, info:any) => {
    console.log('onCheck', checkedKeys, info);
    this.props.patchMenuInfo(checkedKeys)
  };

  public render() {
    const { getFieldDecorator } = this.props.form;
    const detailInfo = this.props.detailInfo;

    const formItemLayout = {
      labelCol: { span: 5 },
      wrapperCol: { span: 19 }
    }

    return (
      <Form layout="horizontal" { ...formItemLayout }>
        <FormItem label="角色名称">
          {
            getFieldDecorator('role_name')(
              <Input style={ { width: 120 } } type="text" placeholder={ detailInfo.role_name } disabled={true} />
            )
          }
        </FormItem>
        <FormItem label="状态">
          {
            getFieldDecorator('status', {
              initialValue: 1
            })(
              <Select style={ { width: 120 } }>
                <Option value={ 1 }>启用</Option>
                <Option value={ 0 }>禁用</Option>
              </Select>
            )
          }
        </FormItem>
        <Tree
          checkable={true}
          defaultExpandAll={true}
          onCheck={ this.onCheck }
          checkedKeys={ this.props.menuInfo }
        >
          <TreeNode title="平台权限" key="platform_all">
            { this.renderThreeNodes(menuConfig) }
          </TreeNode>
        </Tree>
      </Form>
    )
  }
}