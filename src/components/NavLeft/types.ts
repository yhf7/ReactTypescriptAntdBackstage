export interface IProps extends Typings.RouteProps<any, any> {
  menuType?: boolean;
  menuName?: string;
  dispatch?: any;
  switchMenus?: any;
  params?:any
}

// tslint:disable-next-line:no-empty-interface
export interface IState {
  currentKey?: string[];
  menuTreeNode: any;
  selectekey: string[]
}

// 左侧列表循环接口
export interface IMenuData { title: string; key: string; children?: any[]; }