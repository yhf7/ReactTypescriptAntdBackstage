export interface IProps extends Typings.RouteProps<any, any> {
  menuType?: boolean;
  menuName?: string;
}

// tslint:disable-next-line:no-empty-interface
export interface IState {
  userName: string,
  sysTime: any,
  dayPictureUrl: any,
  weather: any
}
