export interface IProps extends Typings.RouteProps<any, any> {
  menuType?: string;
  menuName?: string;
  dispatch?: any;
  userName?: string,
  SignOut?:any
}

// tslint:disable-next-line:no-empty-interface
export interface IState {
  sysTime: any,
  dayPictureUrl: any,
  weather: any
  setI?:any
}

// 左侧列表循环接口
export interface IMenuData { title: string; key: string; children?: any[]; }