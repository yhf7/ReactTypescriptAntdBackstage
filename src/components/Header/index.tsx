import API from "@/API";
// 引入方法
import { signOut } from '@/store/actions'
import Util from "@/utils/utils";
import { Col, Row } from 'antd';
import React, { Component } from 'react'
import { connect } from 'react-redux';
import { Link } from "react-router-dom";
import { Dispatch } from 'redux';
import './index.less'
import { IProps, IState } from './types';// interface 接口

const mapStateToProps = (state: Typings.StoreState) => ({
  // tutor: state.tutor.user,
  menuName: state.menuName,
  userName: state.username
});

const mapDispatchToProps = (dispatch: Dispatch<Typings.IndexActions>) => ({
  // verifyUser: (params: Typings.VerifyParmas) => dispatch(UserActions.verifyUser(params)),
  SignOut: () => dispatch(signOut()),
});

@(connect(mapStateToProps, mapDispatchToProps) as any)
export default class Header extends Component<IProps,IState> {
  public state: IState
  constructor(props: Readonly<IProps>) {
    super(props);
    this.state = {
      sysTime: null,
      dayPictureUrl: null,
      weather: null
    };
  }

  // 组件已进入就执行调函数
  public componentDidMount() {
    // 开启一个定时器，用来实现动态时间
    const setI = setInterval(() => {
      // 请求封装在util的时间处理函数
      const sysTime = Util.formateDate(new Date().getTime())
      // 并设置新的时间
      this.setState({
        sysTime
      })
    }, 1000);

    this.setState({
      setI
    })
    // 调用请求天气的函数
    this.getWeatherAPIData()
  }

  public onsignOut = () => {
    clearInterval(this.state.setI);
    this.props.SignOut()
  }

  // 封装的请求天气api的函数
  public getWeatherAPIData() {
    // 定义城市
    const city:string = 'dongguan'
    // 调用axios封装的函数
    API.getWeather({
      url: encodeURIComponent(city) + '&output=json&ak=3p49MVra6urFRGOT9s8UBWr2'
      // url: 'https://www.tianqiapi.com/api/?version=v1&city=东莞'
    }).then((res:any) => {
      // 判断数据是否请求成功
      if (res.status === 'success') {
        const data:any = res.results[0].weather_data[1]// 拿取数据并设置
        this.setState({
          dayPictureUrl: data.dayPictureUrl,
          weather: data.weather
        })
      }
    })
  }

  public render(): React.ReactChild {
    const menuType = this.props.menuType;
    return (
      <div className="header">
        {/* 头部 */ }
        <Row className="header-top">
          {
            menuType ? <Col span={ 6 } className="logo">
              <img src="assets/logo-ant.svg" alt="" />
              <span>IMooc 通用管理系统</span>
            </Col> : ""
          }
          {/* 采用珊格 */ }
          <Col span={ menuType ? 18 : 24 }>
            <span>欢迎，{ this.props.userName }</span>
            <Link onClick={this.onsignOut} to="/Login">退出</Link>
          </Col>
        </Row>
        {/* 内容面包屑，判断如果有menuType就不显示没有就显示 */ }
        {
          menuType ? '' : <Row className="breadcrumb">
            <Col span={ 3 } className="breadcrumb-title">
              { this.props.menuName }
            </Col>
            {/* 这个就内容 */ }
            <Col span={ 21 } className="weather">
              {/* 时间 */ }
              <span className="date">
                { this.state.sysTime }
              </span>
              {/* 这两个上天气的图片和天气 */ }
              <span className="weather-img">
                <img src={ this.state.dayPictureUrl } alt="" />
              </span>
              <span className="weather-detail">
                { this.state.weather }
              </span>
            </Col>
          </Row>
        }
      </div>
    )
  }
}