import { Table } from 'antd'
import React from 'react'
import  "./index.less"
import { IProps, IState } from "./types";

export default class ETable extends React.Component<IProps, IState> {

    public state: IState
    constructor(props: Readonly<IProps>) {
        super(props);
        this.state = {

        }
    }
    
    // 处理行点击事件
    public onRowClick = (record:any, index:number) => {
        const rowSelection = this.props.rowSelection;
        if(rowSelection == 'checkbox'){
            let selectedRowKeys = this.props.selectedRowKeys;
            let selectedIds = this.props.selectedIds;
            let selectedItem = this.props.selectedItem || [];
            if (selectedIds) {
                const i = selectedIds.indexOf(record.id);
                if (i == -1) {// 避免重复添加
                    selectedIds.push(record.id)
                    selectedRowKeys.push(index);
                    selectedItem.push(record);
                }else{
                    selectedIds.splice(i,1);
                    selectedRowKeys.splice(i,1);
                    selectedItem.splice(i,1);
                }
            } else {
                selectedIds = [record.id];
                selectedRowKeys = [index]
                selectedItem = [record];
            }
            this.props.updateSelectedItem(selectedRowKeys,selectedItem || {},selectedIds);
        }else{
            const selectKey = [index];
            const selectedRowKeys = this.props.selectedRowKeys;
            if (selectedRowKeys && selectedRowKeys[0] == index){
                return;
            }
            this.props.updateSelectedItem(selectKey,record || {});
        }
    };

    // 选择框变更
    public onSelectChange = (selectedRowKeys: any, selectedRows: any[]) => {
        const rowSelection = this.props.rowSelection;
        const selectedIds: any[] = [];
        if(rowSelection == 'checkbox'){
            selectedRows.map((item:{id?:string}) => {
                return selectedIds.push(item.id);
            });
            this.setState({
                selectedRowKeys,
                selectedIds,
                selectedItem: selectedRows[0]
            });
        }
        this.props.updateSelectedItem(selectedRowKeys,selectedRows[0],selectedIds);
    };

    public onSelectAll = (selected: any, selectedRows: any[], changeRows: any) => {
        const selectedIds: any[] = [];
        const selectKey: any[] = [];
        selectedRows.forEach((item: { id?: string; },i: any)=> {
            selectedIds.push(item.id);
            selectKey.push(i);
        });
        this.props.updateSelectedItem(selectKey,selectedRows[0] || {},selectedIds);
    }

    public getOptions = () => {
        const p = this.props;
        enum nameList {
            "订单编号" = 170,
            "车辆编号" = 80,
            "手机号码" = 96,
            "用户姓名" = 70,
            "密码" = 70,
            "运维区域" = 300,
            "车型" = 42,
            "故障编号" = 76,
            "代理商编码" = 97,
            "角色ID" = 64
        };
        if (p.columns && p.columns.length > 0) {
            p.columns.forEach((item: { title: any; width: string | number; className: string; bordered: boolean; })=> {
                // 开始/结束 时间
                if(!item.title){
                    return
                }
                if(!item.width){
                    if(item.title.indexOf("时间") > -1 && item.title.indexOf("持续时间") < 0){
                        item.width = 132
                    }else if(item.title.indexOf("图片") > -1){
                        item.width = 86
                    }else if(item.title.indexOf("权限") > -1 || item.title.indexOf("负责城市") > -1){
                        item.width = '40%';
                        item.className = "text-left";
                    }else{
                        if(nameList[item.title]){
                            item.width = nameList[item.title];
                        }
                    }
                }
                item.bordered = true;
            });
        }
        const { selectedRowKeys } = this.props;
        const rowSelection = {
            type: 'radio',
            selectedRowKeys,
            onChange: this.onSelectChange,
            onSelect:(record: any, selected: any, selectedRows: any)=>{
                console.log('...')
            },
            onSelectAll:this.onSelectAll
        };
        let RowSelection:any|null = this.props.rowSelection;
        // 当属性未false或者null时，说明没有单选或者复选列
        if(RowSelection===false || RowSelection === null){
            RowSelection = false;
        }else if(RowSelection == 'checkbox'){
            // 设置类型未复选框
            rowSelection.type = 'checkbox';
        }else{
            // 默认未单选
            RowSelection = 'radio';
        }
        return <Table 
                className="card-wrap page-table"
                bordered={true} 
                {...this.props}
                dataSource={ this.props.dataSource }
                pagination={ this.props.pagination }
                rowSelection={RowSelection?rowSelection:null as any}
                onRow={(record,index) => ({
                    onClick: ()=>{
                        if(!RowSelection){
                            return;
                        }
                        this.onRowClick(record,index)
                    }
                  })}
            />
    };
    public render = () => {
        return (
            <div>
                {this.getOptions()}
            </div>
        )
    }
}