export interface IProps extends Typings.RouteProps<any, any> {
  rowSelection?:any|null
  selectedRowKeys?:any
  selectedIds?:any
  selectedItem?:any
  updateSelectedItem: updateSelectedItem
  columns?:any[];
  dataSource:any
  pagination:any
}

// tslint:disable-next-line:no-empty-interface
export interface IState {
  selectedRowKeys?:any,
  selectedIds?:any,
  selectedItem?:any
}

type updateSelectedItem = (selectedRowKeys: any, selectedItem:any, selectedIds?:any) => void;