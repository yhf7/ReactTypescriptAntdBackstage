export interface IProps extends Typings.RouteProps<any, any> {
  form?:any
  userInfo:{username:string,sex:number,state:number,birthday:string,address:string}
  type:any
  wrappedComponentRef?: (fieldsValue: any) => void
}
