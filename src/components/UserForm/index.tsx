
import { DatePicker, Form, Input, Radio, Select } from 'antd'
import RadioGroup from 'antd/lib/radio/group'
import Moment from 'moment'
import React, { Component } from 'react'
import {IProps} from './types'
const FormItem = Form.Item;
const TextArea = Input.TextArea;
const { Option } = Select;


@(Form.create({}) as any)
export default class UserForm extends Component<IProps,{}> {
  

  public getState = (state:number) => {
    enum config {
      '咸鱼一条',
      '风华浪子',
      '北大才子一枚',
      '百度FE',
      '创业者'
    }
    return config[state]
  }

  public render() {
    const { getFieldDecorator } = this.props.form;
    const userInfo = this.props.userInfo || {};
    const type = this.props.type;

    const formItemLayout = {
      labelCol: { span: 5 },
      wrapperCol: { span: 19 }
    }
    return (
      <Form layout="horizontal" { ...formItemLayout }>
        <FormItem label="用户名">
          {
            userInfo && type == 'detail' ? userInfo.username : getFieldDecorator('user_name', {
              initialValue: userInfo.username,
              rules: [
                {
                  required: true,
                  message: '请输入用户名',
                },
              ],
            })(
              <Input type="text" placeholder="请输入用户名" />
            )
          }
        </FormItem>
        <FormItem label="性别">
          {
            userInfo && type == 'detail' ? userInfo.sex == 1 ? '男' : '女' : getFieldDecorator('sex', {
              initialValue: userInfo.sex
            })(
              <RadioGroup>
                <Radio value={ 1 }>男</Radio>
                <Radio value={ 2 }> 女</Radio>
              </RadioGroup>
            )
          }
        </FormItem>
        <FormItem label="状态">
          {
            userInfo && type == 'detail' ? this.getState(userInfo.state) : getFieldDecorator('state', {
              initialValue: userInfo.state
            })(
              <Select style={ { width: 120 } }>
                <Option value={ 1 }>咸鱼一条</Option>
                <Option value={ 2 }>风华浪子</Option>
                <Option value={ 3 }>北大才子</Option>
                <Option value={ 4 }>百度FE</Option>
                <Option value={ 5 }>创业者</Option>
              </Select>
            )
          }
        </FormItem>
        <FormItem label="生日">
          {
            userInfo && type == 'detail' ? userInfo.birthday : getFieldDecorator('birthday', {
              initialValue: Moment(userInfo.birthday)
            })(
              <DatePicker />
            )
          }
        </FormItem>
        <FormItem label="联系地址">
          {
            userInfo && type == 'detail' ? userInfo.address : getFieldDecorator('address', {
              initialValue: userInfo.address,
              rules: [
                {
                  required: true,
                  message: '请输入地址信息',
                },
              ],
            })(
              <TextArea rows={ 3 } placeholder="请输入联系地址" />
            )
          }
        </FormItem>
      </Form>
    )
  }
}
