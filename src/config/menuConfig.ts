// 左侧列表数据
const menuList: any[] = [
  {
    key: '/admin/home',
    title: '首页',
  },
  {
    key: '/admin/ui',
    title: 'UI',
    children: [
      {
        key: '/admin/ui/buttons',
        title: '按钮',
      },
      {
        key: '/admin/ui/modals',
        title: '弹框',
      },
      {
        key: '/admin/ui/loadings',
        title: 'Loading',
      },
      {
        key: '/admin/ui/notification',
        title: '通知提醒',
      },
      {
        key: '/admin/ui/messages',
        title: '全局Message',
      },
      {
        key: '/admin/ui/tabs',
        title: 'Tab页签',

      },
      {
        key: '/admin/ui/gallery',
        title: '图片画廊',

      },
      {
        key: '/admin/ui/carousel',
        title: '轮播图',

      }
    ]
  },
  {
    key: '/admin/form',
    title: '表单',

    children: [
      {
        key: '/admin/form/login',
        title: '登录',

      },
      {
        key: '/admin/form/reg',
        title: '注册',

      }
    ]
  },
  {
    key: '/admin/table',
    title: '表格',

    children: [
      {
        key: '/admin/table/basic',
        title: '基础表格',

      },
      {
        key: '/admin/table/high',
        title: '高级表格',

      }
    ]
  },
  {
    key: '/admin/rich',
    title: '富文本',

  },
  {
    key: '/admin/city',
    title: '城市管理',

  },
  {
    key: '/admin/order',
    title: '订单管理',

    btnList: [
      {
        key: '/admin/detail',
        title: '订单详情',

      },
      {
        key: '/admin/finish',
        title: '结束订单',

      }
    ]
  },
  {
    key: '/admin/user',
    title: '员工管理',

  },
  {
    key: '/admin/bikeMap',
    title: '车辆地图',

  },
  {
    key: '/admin/charts',
    title: '图表',

    children: [
      {
        key: '/admin/charts/bar',
        title: '柱形图',

      },
      {
        key: '/admin/charts/pie',
        title: '饼图',

      },
      {
        key: '/admin/charts/line',
        title: '折线图',

      },
    ]
  },
  {
    key: '/admin/permission',
    title: '权限设置',

  },
];
export default menuList;