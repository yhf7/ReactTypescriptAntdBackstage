export type DEMO = 'DEMO'

export interface FetchMiddleWare {
    stage?: string;
    result?: any;
    originalType?: string;
    error?: any;
    menuName?: string
}

export type IndexActions = IndexAction;

export interface IndexAction extends FetchMiddleWare {
    type: any;
    menuName?: string;
    login?: boolean
    username?: string
}

