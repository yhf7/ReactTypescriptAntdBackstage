// 图表
// 柱状图
import Bar from '@/pages/charts/bar'
// 折线图
import Line from '@/pages/charts/line'
// 饼图
import Pie from '@/pages/charts/pie'
import City from '@/pages/city'// 城市管理
import FormLogin from "@/pages/form/Login"; // 表单登录
import FormRegister from "@/pages/form/register"; // 表单注册
import Home from "@/pages/home";// 首页
import BikeMap from "@/pages/map";
import OrderDetail from '@/pages/order/detail'// 
import Order from '@/pages/order/index'// 订单管理
// 权限控制
import Permission from '@/pages/permission'
import Rich from '@/pages/rich'// 富文本
import BasicTable from '@/pages/table/basicTable' // 普通表格
import HighTable from "@/pages/table/highTable"; // 高级表格
import Buttons from '@/pages/ui/buttons'// ui 按钮
import Carousel from '@/pages/ui/carousel'// ui
import Gallery from '@/pages/ui/gallery'// ui
import Loadings from '@/pages/ui/loadings'// ui
import Messages from '@/pages/ui/messages'// ui
import Modals from '@/pages/ui/modals'// ui
import Notification from '@/pages/ui/notification'// ui
import Tabs from '@/pages/ui/tabs'// ui
import User from "@/pages/user";
import Admin from "@/view/admin";// admin视图
import App from "@/view/App"; // 全局视图
import Common from '@/view/common'// 单车地图视图
import Login from "@/view/logins";
import * as React from "react";
import { connect } from 'react-redux';
import { BrowserRouter as Router, Redirect, Route, Switch } from "react-router-dom";
import { Dispatch } from 'redux';
import { IProps } from "./types";

const mapStateToProps = (state: Typings.StoreState) => ({
  // tutor: state.tutor.user,
  login: state.login
});

const mapDispatchToProps = (dispatch: Dispatch<Typings.IndexActions>) => ({
  // verifyUser: (params: Typings.VerifyParmas) => dispatch(UserActions.verifyUser(params)),
});

@(connect(mapStateToProps, mapDispatchToProps) as any)
export default class Routers extends React.Component<IProps, {}> {
  public render() {
    // 自行创建
    return (
      <Router>
        <App>
          <Switch>
            <Route path="/Login" component={ Login } />
            {/* 不添加大括号自动添加return */ }
            <Route path="/common" render={ () =>
              <Common>
                <Route path="/common/order/detail/:orderId" component={ OrderDetail } />
              </Common>
            } />
            <Route path="/" render={ () =>
              this.props.login ?
              (<Switch>
                <Route path="/admin" render={ () =>
                  <Admin>
                    <Switch>
                      <Route path="/admin/home" component={ Home } />
                      <Route path="/admin/ui/buttons" component={ Buttons } />
                      <Route path="/admin/ui/modals" component={ Modals } />
                      <Route path="/admin/ui/loadings" component={ Loadings } />
                      <Route path="/admin/ui/notification" component={ Notification } />
                      <Route path="/admin/ui/messages" component={ Messages } />
                      <Route path="/admin/ui/tabs" component={ Tabs } />
                      <Route path="/admin/ui/gallery" component={ Gallery } />
                      <Route path="/admin/ui/carousel" component={ Carousel } />
                      <Route path="/admin/form/login" component={ FormLogin } />
                      <Route path="/admin/form/reg" component={ FormRegister } />
                      <Route path="/admin/table/basic" component={ BasicTable } />
                      <Route path="/admin/table/high" component={ HighTable } />
                      <Route path="/admin/rich" component={ Rich } />
                      <Route path="/admin/city" component={ City } />
                      <Route path="/admin/order" component={ Order } />
                      <Route path="/admin/user" component={ User } />
                      <Route path="/admin/bikeMap" component={ BikeMap } />
                      <Route path="/admin/charts/bar" component={ Bar } />
                      <Route path="/admin/charts/pie" component={ Pie } />
                      <Route path="/admin/charts/line" component={ Line } />
                      <Route path="/admin/permission" component={ Permission } />
                      {/* <Route component={ NoMatch } /> */ }
                      <Redirect to="/admin/home" />
                    </Switch>
                  </Admin>
                } />
                {/* 跟路径默认找不到返回到admin */ }
                <Redirect to="/admin" />
              </Switch>
              )
              : (
                <Switch>
                    <Redirect to="/Login" />
                </Switch>
              )
            } />
          </Switch>
        </App>
      </Router>
    );
  }
}