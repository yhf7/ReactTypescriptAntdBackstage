import Utils from '@/utils/utils'
import { message } from 'antd'
import axios from 'axios'
import Jsonp from 'jsonp'

// import {Modal} from 'antd'

// axios 配置
axios.defaults.timeout = 60000
axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=UTF-8';
export default class Axios {

  /**
   * 公共请求列表封装
   * @param {Object} that 把调用的this传进来
   * @param {Url} url 请求api
   * @param {Object} params 把传给后端的数据传过来
   * @param {Boolean} isMock 判断是否使用mock数据
   */
  public static requestList(that: any, url: string, params?: object, isMock: boolean = true) {
    // 传给后端的数据
    const data: object = {
      params
    }
    // 调用ajax
    this.ajax(url,{
      data,
      isMock
    }).then((res: any) => {

      // 判断是否取到数据
      if (res && res.result) {
        // 循环添加key，表格需要给每个元素配置key
        const list: any = res.result.item_list.map((item: { key: number | null | undefined }, index: number) => {
          item.key = index;
          return item;
        });


        // 设置表格数据以及表格的分页配置，调用Utils中封装的公共配置
        that.setState({
          list,
          pagination: Utils.pagination(res, (current: any) => {
            that.params.page = current;
            that.requestList();
          })
        })
      }
    })
  }

  /**
   * jsonp请求封装
   * @param {*} options 传入对象，url等
   */
  public static jsonp(options: { url: string }) {
    return new Promise((res, rej) => {
      // jsonp解决跨域请求，传入url，和携带回调，通过promise返回
      Jsonp(options.url, {
        param: 'callback'
      }, (err: any, response: any) => {
        res(response)
      })
    })
  }

  /**
   * 封装等axios请求函数
   * @param {Object} options 请求的url以及参数等
   * url 路由
   * data {Object} 传给后端的数据
   * isMock {Boolean} 是否需要mock，不传就用服务器的路由(true为mock)
   * isShowLoading {Boolean} 是否需要loading 默认加载（false为关闭）
   */
  public static ajax(url: string, options: { data: { params?: any; isShowLoading?: boolean; username?: string; password?: string }; isMock?: boolean; }, method: any = "GET") {
    // 定义loading
    let loading: any;
    // api请求地址
    let baseApi: any;
    // 判断是否携带参数data，以及判断是否要关闭loading，如果没有进入设置
    if (options.data && options.data.isShowLoading !== false) {
      // 获取dom中id为ajaxLoading的元素并设置style，这个以及放在了index文件里面了
      loading = document.getElementById('ajaxLoading');
      loading.style.display = 'block'
    }
    if (options.isMock) {
      // 请求的默认地址
      baseApi = 'http://localhost:8090/api'
    } else {
      baseApi = ''
    }
    // let baseApi = 'https://www.easy-mock.com/mock/5a7278e28d0c633b9c4adbd7/api';
    // 返回一个promise
    return new Promise((res, rej) => {
      // 请求axios传入url等以及回调
      axios({
        url,
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        method,
        baseURL: baseApi,
        timeout: 5000,
        params: (method != "POST") ? (options.data && options.data.params) || '':"",
        data: (method == "POST") ? options.data.params || '' : ''
      }).then(response => {
        // 请求完判断关闭loading
        if (options.data && options.data.isShowLoading !== false) {
          loading = document.getElementById('ajaxLoading');
          loading.style.display = 'none'
        }
        // 判断请求成功返回res，非200为错误rej返回，这都是promise里面调元素了
        if (response.status === 200) {
          const resp = response.data
          // if (resp.code === "0") {
          res(resp)
          // } else {
          //   Modal.info({
          //     title: '提示',
          //     content: resp
          //   })
          // }
        } else {
          rej(response)
        }
      }).catch(() => {
        // 请求完判断关闭loading
        if (options.data && options.data.isShowLoading !== false) {
          loading = document.getElementById('ajaxLoading');
          loading.style.display = 'none'
        }
        message.error('数据加载失败'+res);
      })
    })
  }
}