import Axios from './method'

export default {

  // 登录
  getLogin(params: { data: { params: { username: string, password: string} }, isMock: boolean }) { return Axios.ajax('/user/login', params, "POST") },

  // 获取天气
  getWeather(params: { url: string }) { return Axios.jsonp({ url: 'http://api.map.baidu.com/telematics/v3/weather?location=' + params.url }) },
  // 获取表格mock数据
  getTableList(params: { data: { params: { page: number } }, isMock: boolean }) { return Axios.ajax('/table/high/list', params) },

  // 获取管理列表数据
  getOpenCity(that: any, params?: object, isMock?: boolean) { return Axios.requestList(that, '/city/open_city', params, isMock) },
  // 开通城市
  getCityOpen(params: { data: { params?: any}, isMock?: boolean}) { return Axios.ajax('/city/open', params) },


  // 获取订单数据
  getOrderList(that: any, params?: object, isMock?: boolean) { return Axios.requestList(that, '/order/list', params, isMock) },
  getOrderFinish(params: { data: { params?: { orderId?: number } }, isMock?: boolean }) { return Axios.ajax('/order/finish_order',params)},
  getOrderEbike(params: { data: { params?: { orderId?: number } }, isMock?: boolean }) { return Axios.ajax('/order/ebike_info', params) },
  getOrderDetail(params: { data: { params?: { orderId?: number } }, isMock?: boolean }) { return Axios.ajax('/order/detail', params) },

  // user
  getUserAdd(params: { data: { params?: object }, isMock?: boolean }) { return Axios.ajax('/user/add', params) },
  getUserDelete(params: { data: { params?: { id?: number } }, isMock?: boolean }) { return Axios.ajax('/user/delete', params) },
  getUserEdit(params: { data: { params?: object }, isMock?: boolean }) { return Axios.ajax('/user/edit', params) },
  getUserList(that: any, params?: object, isMock?: boolean) { return Axios.requestList(that, '/user/list', params, isMock) },

  // map
  getMapBikeList(params: { data: { params?: object }, isMock?: boolean }) { return Axios.ajax('/map/bike_list', params) },

  // role
  getRoleList(that: any, params?: object, isMock?: boolean) { return Axios.requestList(that, '/role/list', params, isMock) },
  getRoleUserList(params: { data: { params?: { id: number } }, isMock?: boolean }) { return Axios.ajax('/role/user_list', params) },
  getRoleCreate(params: { data: { params?: object }, isMock?: boolean }) { return Axios.ajax('/role/create', params) },
  getRoleUserEdit(params: { data: { params?: object }, isMock?: boolean }) { return Axios.ajax('/role/user_role_edit', params) },

  // permission
  getPermissionEdit(params: { data: { params?: object }, isMock?: boolean }) { return Axios.ajax('/permission/edit', params) },
}