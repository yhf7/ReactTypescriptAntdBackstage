//  事件促发行为

/**
 * Action 类型
 */
export const type = {
  SWITCH_MENU: 'SWITCH_MENU',
  LOGIN_KEY: 'LOGIN_KEY',
  USER_NAME: 'USER_NAME',
  SIGN_OUT: 'SIGN_OUT'
}

/**
 * 切换菜单类型
 * @param {*} menuName 菜单名称
 */
export function switchMenu(menuName: string) {
  return {
    type: type.SWITCH_MENU,
    menuName
  }
}


/**
 * 判断是否登录
 */

export function loginKey(login: boolean) {
  return {
    type: type.LOGIN_KEY,
    login
  }
}


/**
 * 记录登录人的名字
 */

export function userName(username: string) {
  return {
    type: type.USER_NAME,
    username
  }
}

/**
 * 退出登录
 */
export function signOut() {
  return {
    type: type.SIGN_OUT
  }
}