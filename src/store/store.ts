/**
 * 引入createStore
 */

import { createStore } from 'redux'
import reducer from './reducers'

const initialState = {
  menuName: '首页',
  login: false,
  username: ''
}
export default () => createStore(reducer, initialState)