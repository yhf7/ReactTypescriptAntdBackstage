import { type } from '../actions/index';

const defaultData = {
  menuName: '首页',
  login:false,
  username:''
};

export default (state: Typings.StoreState = defaultData, action: Typings.IndexActions): Typings.StoreState => {
  switch (action.type) {
    case type.SWITCH_MENU:
      return {
        ...state,
        menuName: action.menuName
      };
    case type.LOGIN_KEY:
      return {
        ...state,
        login: action.login
      }
      case type.USER_NAME:
        return {
          ...state,
          username: action.username
        }
    case type.SIGN_OUT:
      return {
        ...defaultData
      }
    default:
      return { ...state };
  }
};
