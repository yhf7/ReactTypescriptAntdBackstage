// 底部
import Footer from '@/components/Footer'
// 头部栏
import Header from '@/components/Header/index'
// 左侧边栏
import NavLeft from '@/components/NavLeft/index'
// antd 栅格布局
import { Col, Row } from 'antd';
import React, { Component } from 'react'


// 全局样式
import '@/style/common.less'

export default class Admin extends Component {

  public printLabel = (labelledObj: { label: string }) => {
    // tslint:disable-next-line:no-console
    console.log(labelledObj.label);
  }

  public abc = () => {
    const myObj = { size: 10, label: "Size 10 Object" };
    this.printLabel(myObj)
  }

  public render() {
    return (
      <Row className="container">
        <Col span={ 3 } className="nav-left">
          <NavLeft />
        </Col>
        <Col span={ 21 } className="main">
          <Header />
          <Row className="content">
            { this.props.children }
          </Row>
          <Footer />
        </Col>
      </Row>
    )
  }
}