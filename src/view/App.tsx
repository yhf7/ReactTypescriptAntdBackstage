import React, { Component } from 'react';

class App extends Component {
  public render() {
    return (
      <div className="App">
        { this.props.children }
      </div>
    );
  }
}

export default App;