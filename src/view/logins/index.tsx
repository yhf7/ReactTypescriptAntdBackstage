import API from "@/API";
// 引入方法
import { loginKey,userName } from '@/store/actions'
import { Button, Form, Icon, Input, message } from 'antd'
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import './logins.less'
import { IProps } from "./types";

// redux 数据部分
const mapStateToProps = (state: Typings.StoreState) => ({
  // tutor: state.tutor.user,
});

// action 操作部分
const mapDispatchToProps = (dispatch: Dispatch<Typings.IndexActions>) => ({
  // verifyUser: (params: Typings.VerifyParmas) => dispatch(UserActions.verifyUser(params)),
  loginKeys: (params: boolean) => dispatch(loginKey(params)),
  userNames: (params: string) => dispatch(userName(params)),
});

@(Form.create() as any)
@(connect(mapStateToProps, mapDispatchToProps) as any)
export default class Login extends Component<IProps, {}> {

  /**
   * 点击登录时执行的函
   * @param e 表单属性
   */
  public handleSubmit = (e: any) => {
    e.preventDefault();  // 阻止页面刷新
    // 获取表单数据
    this.props.form.validateFields((err: any, values: { password: string, username: string }) => {
      if (!err) {
        const username = values.username;
        const password = values.password;
        API.getLogin({
          data: {
            params: {
              username,
              password
            }
          },
          isMock: true
        }).then((res:any) => {
          this.props.loginKeys(true)
          this.props.userNames(res.username)
          this.props.history.push("/admin/home");
          message.success('恭喜'+res.username + '登录成功');
        })
        // message.success('恭喜您登录成功,密码为:' + values.password);
      }
    });
  };

  public render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <div className="Login">
        <div className="Index-content">
          <div className="top">
            <h1>单车后台管理系统</h1>
          </div>
          <div className="con">
            <Form onSubmit={ this.handleSubmit } className="login-form">
              {/* username组件，rules为验证规则 */ }
              <Form.Item>
                { getFieldDecorator('username', {
                  initialValue: '',
                  rules: [
                    {
                      required: true,
                      message: '用户名不能为空'
                    },
                    {
                      min: 4, max: 10,
                      message: '长度不在范围内'
                    },
                    {
                      pattern: new RegExp('^\\w+$', 'g'),
                      message: '用户名必须为字母或者数字'
                    }
                  ],
                })(
                  <Input
                    prefix={ <Icon type="user" style={ { color: 'rgba(0,0,0,.25)' } } /> }
                    placeholder="请输入用户名"
                  />,
                ) }
              </Form.Item>
              <Form.Item>
                { getFieldDecorator('password', {
                  initialValue: '',
                  rules: [{ required: true, message: '密码不能为空' }, {
                    min: 6, max: 15,
                    message: '长度不在范围内'
                  },],
                })(
                  <Input
                    prefix={ <Icon type="lock" style={ { color: 'rgba(0,0,0,.25)' } } /> }
                    type="password"
                    placeholder="请输入密码"
                  />,
                ) }
              </Form.Item>
              <Button type="primary" htmlType="submit" className="login-form-button">
                登录
              </Button>
            </Form>
            <p>温馨提示：</p>
            <p>未登录过的新用户，自动注册</p>
            <p>注册过的用户可凭账号密码登录</p>
          </div>
        </div>
      </div>
    );
  }
}