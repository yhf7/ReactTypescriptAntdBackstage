export interface IProps extends Typings.RouteProps<any, any> {
  form?: any
  history:any
  loginKeys: (params:boolean) => void
  userNames: (params:string) => void
}
