import Header from '@/components/Header'
import '@/style/common.less'
import { Row } from 'antd';
import React from 'react'

export default class Common extends React.Component {

  public render() {
    return (
      <div className="container">
        <Row className="simple-page">
          <Header menuType="second" />
        </Row>
        <Row className="content">
          { this.props.children }
        </Row>
      </div>
    );
  }
}
